import {InfoGroup} from "../components/InfoGroup.js"
import {ImagedInfoGroup} from "../components/ImagedInfoGroup.js"
import {SimpleButton} from "../components/SimpleButton.js"
import {ConnectedDropdown} from "../components/ConnectedDropdown.js"
import {CustomDropdown} from "../components/CustomDropdown.js"


export class PageForm extends HTMLElement{

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style> 

                page-form{
                    padding: var(--verticalPadding) var(--sidePadding);
                    display: block;
                    background-color:white;
                }

                page-form .title{
                    margin-bottom: 2em;
                }

                page-form h1{
                    font-size: var(--textSizeSmallerMajor);
                    color:var(--colorLightBlue);
                    font-weight: 900;
                    text-align: center;
                    margin: .2em;
                }
                

                page-form .text{
                    font-size: var(--textSizeBody);
                    color:var(--colorBlack);
                    text-align:justify;
                    margin-bottom: 4em;
                }

                page-form{
                    font-family: Gotham;
                    font-size: var(--textSizeBody);
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                }

                page-form input{
                    box-sizing: border-box;
                    font-size: var(--textSizeBody);
                    outline: none;
                    color:var(--colorBlack)
                }

             

                .input-text:not(:last-child), .input-radio:not(:last-child), .input-upload:not(:last-child), .input-cityState:not(:last-child){
                    margin-bottom: 1.5em;
                }

                label{
                    display:block;
                    color: var(--colorLightBlue);
                }

                label:not(:last-child), .radio label{
                    margin-bottom: .5em;
                }

                .member-id{
                    text-align:right;
                }

                page-form input[type="text"]{
                    padding: .5em;
                    border: .05em solid var(--colorLightBlue);
                    box-sizing: border-box;
                    display: block;
                    width:100%;
                }

                page-form [required]>label:after{
                    content:" *";
                    color: var(--colorBlack);
                }


                /* ESTILOS DE RADIO */ 

                page-form input[type="radio"]{
                    position:absolute;
                    width: 0;
                    height: 0;
                    opacity:0;
                }

                page-form input[type="radio"]+label{
                    display: inline-block;
                    line-height: 1em;
                    
                }

                page-form input[type="radio"]+label:before{
                    content: " ";
                    width:1em;
                    height: 1em;
                    display: inline-block;
                    border: .05em solid var(--colorLightBlue);
                    top:.2em;
                    position:relative;
                    margin-right: .3em;
                }

                page-form input[type="radio"]:checked+label:before{
                    background-color: var(--colorLightBlue);
                }

                page-form .input-radio .options{
                    display: flex;
                    flex-direction: column;
                    max-height: 6em;
                    flex-wrap: wrap;
                }

                connected-dropdown{
                    display: grid;
                    grid-gap:1em;
                    grid-template-columns: 4em 1fr;
                }

                custom-dropdown{
                    position:relative;
                    --myBorder:1px;
                }

                custom-dropdown [slot=field] input{
                    position: relative;
                    margin:0;
                }

                custom-dropdown [slot=list]{
                    background-color: var(--colorLightBlue);
                    position: absolute;
                    top:calc(100% - var(--myBorder));
                    left:0;
                    width:100%;
                    max-height:10em;
                    overflow:hidden;
                    overflow-y:auto;
                    border: var(--myBorder) solid var(--colorLightBlue);
                    box-sizing:border-box;
                }

                
                custom-dropdown .item{
                    padding: .5em calc(.5em  - var(--myBorder));
                    background-color: white;
                    color:var(--colorBlack);
                }
                
                
                custom-dropdown [slot=list]>*:hover, custom-dropdown [slot=list]>*[hover]{
                    background-color: rgba(255,255,255, .8);
                }

                custom-dropdown [dropbutton]{
                    width:1em;
                    height:1em;
                    position:absolute;
                    right: .5em;
                    top:.5em;
                    fill: var(--colorBlack);
                    opacity:.5;
                }

                .upload-container, .button{
                    padding: .5em;
                    margin-top:.5em;
                    
                    box-sizing: border-box;
                    width:100%;
                    position: relative;
                    display: grid;
                    grid-template-columns: 1fr min-content;
                    align-content: center;
                    justify-content: center;
                    align-items: center;
                    justify-items: center;
                    color:white;
                    background-color:var(--colorLightBlue);
                    cursor:pointer;
                    -webkit-touch-callout: none; /* iOS Safari */
                    -webkit-user-select: none; /* Safari */
                    -khtml-user-select: none; /* Konqueror HTML */
                    -moz-user-select: none; /* Firefox */
                        -ms-user-select: none; /* Internet Explorer/Edge */
                            user-select: none
                }

                .upload-container svg{
                    fill: white;
                    width: 1em;
                    height: 1em;
                }

                .upload-container > label, .button > label{
                    color:white;
                }

                .upload-container > input{
                    position:absolute;
                    opacity:0;
                    left:0;
                    top:0;
                    width:100%;
                    height:100%;
                }

                .groups, .member{
                    border: .05em solid var(--colorLightBlue);
                    padding: .5em; 
                }

                .groups, .member:not(:last-child){
                    margin-bottom: .5em;
                }

                .group .input-text{
                    margin-bottom:.5em;
                }

                .group label{
                    margin-bottom:.2em;
                }

                #revenues_yearly{
                    padding-left: 2.05em;
                }

                .fixed-label{
                    position: absolute;
                    margin-top: calc(-2em - (2 * 0.05em));
                    padding: calc(.5em);
                    margin-left: -0.05em;
                    color: #bbb;
                }

                *[error]{
                    --colorLightBlue:red;
                    --colorBlack:red;
                }
                
                
            </style>

            <section>
                <div class="form">
                    <div class="title">
                        <h1>Desafio Neve</h1>
                        <h1>Programa de Aceleração</h1>
                    </div>
                    <div class="text">
                        <p>
                            O Desafio Neve é uma chamada da Kimberly-Clark para iniciativas inovadoras que atuam
                            na melhoria do saneamento no Brasil, em especial com foco em coleta e tratamento de
                            esgoto.
                        </p>
                        <p>
                            Os participantes selecionados nesta chamada passarão por um Programa de
                            Aceleração que visa estruturar melhor as iniciativas, tanto do ponto de vista do impacto gerado, quanto da
                            sustentabilidade financeira.
                            <br />
                            <br />
                            De 14/10/2019 à 19/02/2020. Com pausa de fim de ano entre 22/12/2019 à 12/01/2020.
                        </p>
                        <p>
                            O programa será conduzido com uma mistura de momentos presenciais (workshops de 2 à
                            3 dias seguidos) e virtuais (webinars e calls), sob responsabilidade do Sense-Lab.
                        </p>
                        <p>
                            Não há custo para a inscrição na chamada, da mesma forma que as iniciativas
                            selecionadas terão todos os custos de deslocamento pagos pelo programa.
                        </p>
                        <p>
                            Ficou com dúvida? <a href="mailto:desafioneve@sense-lab.com">desafioneve@sense-lab.com</a>
                        </p>
                        </p>
                    </div>

                    <form action="#" method="post" id="challengeForm">
                        <input type="hidden" name="csrf" value="">
                        <div required class="input-text">
                            <label for="org_name">Nome da Iniciativa (negócio ou projeto)</label>
                            <input type="text" name="org_name" id="org_name">
                        </div>

                        <div required class="input-text">
                            <label for="resp_name">Nome completo do responsável pela inscrição</label>
                            <input type="text" name="resp_name" id="resp_name">
                        </div>

                        <div required class="input-radio">
                            <label >Como você se insere na iniciativa  </label>
                            <div class="options">
                                <div class="radio">
                                    <input type="radio" id="founder" name="occupation" value="founder" checked="">
                                    <label for="founder">Fundador</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="partner" name="occupation" value="partner">
                                    <label for="partner">Sócio</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="member" name="occupation" value="member">
                                    <label for="member">Membro da Equipe</label>
                                </div>
                            </div>
                        </div>

                        <div required class="input-text small">
                            <label for="telephone">Telefone de contato</label>
                            <input type="text" name="telephone" id="telephone" maxlength="15" placeholder="(11) 99999-9999">
                        </div>

                        <div required class="input-text small">
                            <label for="email">E-mail de contato</label>
                            <input type="text" name="email" id="email" placeholder="meunome@empresa.com">
                        </div>
                        
                        <div required class="input-cityState input-text">
                            <label for="city">Cidade onde a iniciativa está baseada</label>

                            <select name="state" id="state" onchange="updateSelectCities()">
                                <option value="">-- Selecione o Estado --</option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="RS">RS</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>

                            <select name="city" id="city">
                                <option value="">-- Selecione uma Cidade --</option>
                            </select>

                        </div>

                        <div class="input-text small">
                            <label for="founded">Ano de fundação</label>
                            <input type="text" name="founded" id="founded">
                        </div>

                        <!-- <h3>Ano de fundação</h3> -->
                        <div class="input-text">
                            <label for="juridical_format">Formato Jurídico (em caso de projeto dentro do portfólio de uma organização, indicar apenas Projeto)</label>
                            <input type="text" class="big" name="juridical_format" id="juridical_format">
                        </div>

                        <div required class="input-text">
                            <label for="org_description">Breve descrição da iniciativa</label>
                            <input type="text" name="org_description" id="org_description">
                        </div>

                        <div class="input-radio">
                            <label nn>Estágio de maturidade</label>
                            <div class="options">
                                <div class="radio">
                                    <input type="radio" id="idea" name="status" value="idea" checked="">
                                    <label for="idea">Ideia</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="validation" name="status" value="validation">
                                    <label for="validation">Validação</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="prototype" name="status" value="prototype">
                                    <label for="prototype">Protótipo</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="mvp" name="status" value="mvp">
                                    <label for="mvp">MVP</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="traction" name="status" value="traction">
                                    <label for="traction">Tração</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="preScale" name="status" value="pre-scale">
                                    <label for="preScale">Pré-escala</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="scale" name="status" value="scale">
                                    <label for="scale">Escala</label>
                                </div>
                            </div>
                        </div>

                        <div required class="input-text">
                            <label for="experience_description">Já realizou protótipo ou piloto da sua solução? Se sim, descrever a experiência e os resultados obtidos</label>
                            <input type="text" class="big" name="experience_description" id="experience_description">
                        </div>

                        <div required class="input-text">
                            <label for="sustainability_description">Modelo de sustentabilidade financeira (atual ou prevista):  </label>
                            <input type="text" name="sustainability_description" id="sustainability_description">
                        </div>

                        <div required class="input-text">
                            <label for="sponsors_segment">Segmento de clientes ou financiadores (atual e/ou previstos):  </label>
                            <input type="text" name="sponsors_segment" id="sponsors_segment">
                        </div>

                        <div required class="input-text">
                            <label for="capitalization_description">Já realizou venda do produto/serviço ou realizou captação de recursos? <br>
                                Em caso de captação, indicar qual o montante captado e para qual fim foi utilizado 
                            </label>
                            <input type="text" class="big" name="capitalization_description" id="capitalization_description">
                        </div>

                        <!-- <h3>Faturamento anual</h3> -->
                        <div class="input-text fixed">
                            <label for="revenues_yearly">Faturamento anual:</label>
                            <input type="text" name="revenues_yearly" id="revenues_yearly" maxlength="22" >
                            <div class="fixed-label">R$</div>
                        </div>

                        <div required class="input-text">
                            <label for="social_impact_description">Qual o impacto social e/ou ambiental a iniciativa busca gerar e como?  </label>
                            <input type="text" class="big" name="social_impact_description" id="social_impact_description">
                        </div>

                        <div required class="input-text">
                            <label for="target_description">Qual o perfil do público que sua iniciativa beneficia? Indicar, por exemplo, região geográfica e nível socioeconômico.</label>
                            <input type="text" class="big" name="target_description" id="target_description">
                        </div>

                        <div class="input-upload">
                            <label for="upload_project">Upload do Projeto em PDF</label>
                            <div class="upload-container">
                                <input type="file" id="upload_project" name="upload_project" accept=".pdf">
                                <div class="label">Arquivo</div>

                                <svg dropbutton class="svg" >
                                    <use xlink:href="./assets/upload-icon.svg#upload-icon"></use>
                                </svg>
                            </div>
                            <div class="file-name"></div>
                        </div>

                        <div class="input-text groups">
                            <label>Dados da equipe</label> 
                            <fieldset class="member">
                                <label class="member-id">Membro <span class="id">1</span></label>
                                <div class="group">
                                    <div class="input-text">
                                        <label for="name">Nome completo:</label>
                                        <input type="text" name="name" id="name">
                                    </div>
                                    <div class="input-text">
                                        <label for="residence">Local de residência:</label>
                                        <input type="text" name="residence" id="residence">
                                    </div>
                                    <div class="input-text">
                                        <label for="professional_historic">Breve histórico profissional:</label>
                                        <input type="text" name="professional_historic" id="professional_historic">
                                    </div>
                                    <div class="input-text">
                                        <label for="expertises">Expertises:</label>
                                        <input type="text" name="expertises" id="expertises">
                                    </div>
                                    <div class="input-text">
                                        <label for="responsabilities">Responsabilidades aportadas na iniciativa:</label>
                                        <input type="text" name="responsabilities" id="responsabilities">
                                    </div>
                                    <div class="input-text">
                                        <label for="dedication_time">Dedicação atual (em horas/semanas):</label>
                                        <input type="text" name="dedication_time" id="dedication_time">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="button" id="add">
                                <label>Adicionar membro</label>
                            </div>
                        </div>

                        

                        <div required class="input-text">
                            <label for="challenges_description">Quais os principais desafios e necessidades atualmente?  </label>
                            <input type="text" class="big" name=" challenges_description" id="challenges_description">
                        </div>

                        <div required class="input-text">
                            <label for="incubation_process">Já passou por algum processo de Incubação/Aceleração? Se sim, indicar principais pontos positivos e negativos da experiência. </label>
                            <input type="text" class="big" name="incubation_process" id="incubation_process">
                        </div>

                        <div required class="input-text">
                            <label for="motivation_description">Indicar o motivo da inscrição nesse Programa de Aceleração e qual sua importância para o futuro da iniciativa. </label>
                            <input type="text" class="big" name="motivation_description" id="motivation_description">
                        </div>

                        <div class="input-radio">
                            <label nn>Concorda em participar das atividades presenciais em São Paulo?
                                </label>
                            <div class="options">
                                <div class="radio">
                                    <input type="radio" id="yes" name="agree_activities" value="true" checked="">
                                    <label for="yes">Sim</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="no" name="agree_activities" value="false">
                                    <label for="no">Não</label>
                                </div>
                            </div>
                        </div>


                        <div class="button" id="submit">
                            <label>Enviar</label>
                        </div>
                    </form>
                </div>
            </section>
        `;

        this.querySelector("input[type=hidden]").setAttribute("value", window.token);

        this.querySelectorAll("input[type=text]").forEach(element => {
            element.autocomplete = 'off';
        });

        this.memberCount=1;

        this.submitButton=this.querySelector("#submit");
        this.submitButton.addEventListener("click", this.submit.bind(this));
        
        this.memberGroup=this.querySelector(".groups");
        this.memberClone=this.querySelector(".member").cloneNode(true);
        this.add=this.querySelector("#add");

        this.add.addEventListener("click", this.addMember.bind(this));

        this.required=Array.from(this.querySelectorAll("[required]"));
        
        this.required.forEach(element => {
            let inputs=element.querySelectorAll("input");
        });
    }

    addMember(){
        let newClone=this.memberClone.cloneNode(true);
        this.memberCount++;
        newClone.querySelector(".id").innerHTML=this.memberCount;
        this.memberGroup.insertBefore(newClone, this.add);
    }

    submit(){
        this.getFormDatatoJson((resposta)=>{
            var form_data = new FormData();

            for ( var key in resposta ) {
                form_data.append(key, resposta[key]);
            }

            let http = new XMLHttpRequest();
            let url = window.endpoint;

            http.open('POST', url, true);

            http.onreadystatechange = ()=> {
                if (http.readyState === http.DONE) {
                    if (http.status === 201) {
                        Swal.fire(
                            'Sucesso!',
                            'inscrição realizada com sucesso',
                            'success'
                        ).then(function(result){
                            window.location.pathname="/"
                        })
                    }else{
                        this.resposta(http.responseText)
                    }
                   
                }
            }
            http.send(form_data);
            
        });
        
    }

    resposta(e){

        let r=JSON.parse(e);

        for(let k in r){
            var el=this.querySelector("#"+k+", [name="+k+"]");

            let check=el.parentElement;
            for(let i=0; i<5; i++){
                if(check.classList.value.indexOf("input-")>-1){
                    break;
                }else{
                    check=check.parentElement
                }
            }
            check.setAttribute("error", "");
        }

    }

    getFormDatatoJson(callback){
        var formData = new FormData(this.querySelector("form"));
        var object = {members:[]};

        let group=["name","residence","professional_historic","expertises","responsabilities","dedication_time","register_id","created_at]"]

        let count=-1
        formData.forEach((value, key) => {
            if(key==group[0]){
                count++;
            }

            if(group.indexOf(key)>-1){
                
                if(object["members"][count]==undefined){
                    object["members"][count]={}
                }
                object["members"][count][key]=value
            }else{
                object[key] = value;
            }
        });

        object.members=JSON.stringify(object.members);

        callback(object)
    }

}
customElements.define('page-form', PageForm);