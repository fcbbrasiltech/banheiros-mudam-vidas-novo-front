import {InfoGroup} from "../components/InfoGroup.js"
import {ImagedInfoGroup} from "../components/ImagedInfoGroup.js"
import {ReferenceImage} from "../components/ReferenceImage.js"
import {SquareGallery} from "../components/SquareGallery.js"
import {VideoStory} from "../components/VideoStory.js"
import {SimpleButton} from "../components/SimpleButton.js"

export class HomePage extends HTMLElement{
    
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>


                home-page #solucao{
                    background-color: var(--colorDarkBlue);
                    text-align: center;
                    color: white;
                }

                home-page #reinventando{
                    background-color: var(--colorLightBlue);
                    text-align: center;
                    color: var(--colorBlack);
                    position:relative;
                }

                home-page .bodyText{
                    font-weight: 100;
                    line-height: 1.8em;
                    font-size: var(--textSizeBody);
                    text-align: justify;
                }

                home-page section{
                    padding: var(--verticalPadding) var(--sidePadding);
                    display: grid;
                    grid-template-columns: 1fr;
                    grid-gap: var(--verticalPadding);
                    grid-auto-rows: auto;
                    box-sizing: border-box;
                    min-height: var(--minCoverHeight);
                    background-color:white;
                }

                home-page p{
                    margin:0px;
                }

                home-page section > img{
                    max-width: 100%;
                    margin: 0 auto;

                }


                home-page section p b{
                    font-weight: 500;
                }
                

                home-page #metade{
                    background-color: var(--colorLightBlue);
                    
                    font-weight: 900;
                    color: white;
                    overflow: hidden;
                    padding-bottom: var(--sidePadding);
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                }

                home-page #metade p{
                    text-align: center;
                }

                home-page #metade #titulo span{
                    display: block;
                    text-align: center;
                    font-size: var(--textSizeMajor);
                }

                home-page #metade #titulo span:nth-child(1){
                    color: white;
                    margin-left: -2em;
                }

                home-page #metade #titulo span:nth-child(2){
                    color: var(--colorBlack);
                    margin-right: -2em;
                }

                home-page #metade square-gallery{
                    margin-left: calc(var(--sidePadding) * -1);
                    width:calc(100% + (var(--sidePadding) * 2)); 
                    --spacing: var(--sidePadding);
                    --show:0px;
                }

                home-page[desk] #metade square-gallery{
                    --my-display:grid;
                    --my-grid-template-columns: 1fr 1fr;
                    --my-grid-gap: var(--sidePadding);
                    width:100%;
                    margin-left:initial;
                }


                home-page #saneamento{
                    padding-left: 0;
                    padding-right: 0;
                }

                home-page #bottomVideo{
                    padding: var(--sidePadding);
                    align-content:center;
                    background-image:url(./assets/bg-can.jpg);
                    background-size: cover;
                    background-position: center right; 
                    grid-template-rows: 1fr auto 2fr auto;
                    
                }

                home-page #bottomVideo info-group{
                    --firstColor:white;
                    grid-row: 2 / span 1;
                }
                home-page #bottomVideo play-button{
                    width: 4em;
                    height: 4em;
                    grid-row: 4 / span 1;
                    align-self: end;
                    justify-self: end;
                    grid-column: 1 / span 1;
                }


                home-page #reinventando  square-gallery{
                    width:calc(100% + (2 * var(--sidePadding)));
                    left: calc(var(--sidePadding) * -1);
                    --spacing: var(--sidePadding);
                    --show:var(--sidePadding);
                }

                home-page[desk] #reinventando  square-gallery{
                    width: 100%;

                    --my-display:grid;
                    --my-grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));;
                    --my-grid-gap: var(--sidePadding);
                }
               

                home-page #gallery > *{
                    margin:0;
                    margin-bottom:var(--sidePadding);
                }

                home-page #gallery .nome{
                    font-size:var(--textSizeHeading);
                    text-transform: uppercase;
                    font-weight:500;
                    margin-bottom: calc(var(--sidePadding) / 2);
                }

                home-page #gallery .area{
                    font-size:var(--textSizeBody);
                    text-transform: uppercase;
                    font-weight:500;
                }

                home-page #gallery .prof{
                    font-size:var(--textSizeSource);
                    font-weight:500;
                }

                home-page[desk] #gallery [info]{
                    display:none;
                }

                div[equipe]{
                    background-size: cover;
                    background-position:center center;
                    background-repeat: no-repeat;
                    margin-bottom: 60pt;
                    position:relative;
                }

                div[equipe] > .info{
                    position: absolute;
                    font-size: var(--textSizeSource);
                    display:grid;
                    width: 100%;
                    top: calc(100% + 1em);
                }

                :host(:not([desk])) div[equipe] > .info{

                }

                div[equipe] > .info > span {
                    white-space:initial;
                    text-align:center;
                    color:var(--colorBlack);
                    line-height: 1.4em;
                }


            </style>
            <section id="metade">
                <div id="titulo">
                    <span>METADE</span>
                    <span>DE NÓS</span>
                </div>
                <p class="bodyText">
                    Metade dos brasileiros não têm acesso ao saneamento básico. Neve está ajudando a transformar essa realidade.
                    <br>
                    Veja o que já foi feito.
                </p>
                <square-gallery dots>
                    <video-story right imgsrc="./assets/depoimento_1.jpg" youtube="HaaNGrMmGzs">
                        <div slot="name">Manuel</div>
                        <div slot="quote">Nunca tive banheiro. Meu Primeiro Banheiro Foi Esse Aí.</div>
                        <div slot="prof">Morador de Milagres do Maranhão</div>
                    </video-story>
                    <video-story left imgsrc="./assets/depoimento_2.jpg" youtube="wILG1mTATps"/>
                        <div slot="name">Francisco Santana</div>
                        <div slot="quote">Tô me sentindo realizado. Vendo o sorriso no rosto deles.</div>
                        <div slot="prof">Líder Comunitário de Milagres do Maranhão</div>
                    </video-story>
                </square-gallery>
            </section>
            <section id="saneamento">
                <info-group bigger center>
                    <span slot="pre">o saneamento</span>
                    <span slot="major">básico</span>
                    <span slot="pos">no brasil</span>
                    <span slot="text">Entenda qual é a situação e por que esse não se pode mais esperar.</span>
                </info-group>

                <imaged-info-group>
                    <reference-image src="./assets/foto-neve-univef-abastecimento-de-agua-03.png"
                        label="UNICEF/BRZ/RAONI LIBÓRIO">
                    </reference-image>
                    <info-group>
                        <span slot="pre">abastecimento de</span>
                        <span slot="major">água</span>
                        <span slot="text">40 milhões de pessoas não têm água tratada em nosso país.</span>
                        <span slot="source">Fonte: Pesquisa Nacional por Amostra de Domicílios (PNDA), 2014. Índice de acesso à água tratada.</span>
                    </info-group>
                </imaged-info-group>

                <imaged-info-group right>
                    <reference-image src="./assets/foto-neve-univef-tratamento-esgoto-03.png" label="UNICEF/BRZ/RAONI LIBÓRIO">
                    </reference-image>
                    <info-group>
                        <span slot="pre">esgotamento</span>
                        <span slot="major">sanitário</span>
                        <span slot="pos">no brasil</span>
                        <span slot="text">Metade do esgoto gerado no Brasil simplesmente não é coletado, e é despejado irregularmente.</span>
                        <span slot="source">Fonte: Sistema Nacional de Informações sobre Saneamento (SNIS), 2014. Índice de atendimento total de esgoto referido aos municípios atendidos com água (49,84%).</span>
                    </info-group>
                </imaged-info-group>

                <imaged-info-group>
                    <reference-image src="./assets/foto-neve-univef-recolhimento-lixo-03.png" label="UNICEF/BRZ/RAONI LIBÓRIO">
                    </reference-image>
                    <info-group>
                        <span slot="pre">manejo de</span>
                        <span slot="major">resíduos</span>
                        <span slot="text">Mais de 4 milhões de pessoas estão vivendo em domicílio ou propriedade sem banheiro.</span>
                        <span slot="source">Fonte: PNDA, 2014. Índice de domicílios ou propriedades sem banheiro.</span>
                    </info-group>
                </imaged-info-group>
            </section>

            <section id="solucao">
                <info-group bigger dark-bg>
                    <span slot="pre">entenda a</span>
                    <span slot="major">solução</span>
                    <span slot="pos">de neve</span>
                </info-group>

                <img src="./assets/img-bason-mobile.png" alt="">

                <p class="bodyText">
                    Para ajudar uma das regiões com o pior índice de saneamento básico do país, Milagres do Maranhão, Neve desenvolveu um banheiro que independe de esgotamento sanitário e abastecimento de água para funcionar. A solução foi adaptada de um modelo de sanitário seco, conhecido como Bason.
                </p>

                <info-group bigger dark-bg>
                    <span slot="pre">para quem</span>
                    <span slot="pos">mais precisa</span>
                </info-group>

                <p class="bodyText" style="text-align: center">
                    Deseja ajudar uma comunidade próxima de você, replicando o banheiro de Neve?
                </p>

                <simple-button href="./downloads/NeveBanheiroSustentavel.pdf" target="_blank">saiba como</simple-button>
            </section>

            <section id="reinventando">
                <info-group bigger light-bg>
                    <span slot="pre">reinventando o</span>
                    <span slot="major">banheiro</span>
                </info-group>

                <p class="bodyText">
                    Neve recrutou um time multidisciplinar, composto por 8 jovens, e lançou o desafio: desenvolver uma solução que ajudasse a lidar com a falta de saneamento básico em uma das regiões com o pior índice do país.
                </p>

                <div id="gallery">
                    <square-gallery dots>
                        <div equipe style="background-image:url(./assets/neve-francisco.jpg)" alt="Francisco Santana" prof="Líder Comunitário de Milagres do Maranhão" area="Olhar Interno / Imersão">
                            <div class="info">
                                <span>Francisco Santana</span>
                                <span>Líder Comunitário de Milagres do Maranhão</span>
                                <span>Olhar Interno / Imersão</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-leticia.jpg)" alt="Leticia Torres" prof="Administração de Empresas" area="Planejamento">
                            <div class="info">
                                <span>Leticia Torres</span>
                                <span>Administração de Empresas</span>
                                <span>Planejamento</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-guilherme.jpg)" alt="Guilherme Atan" prof="Design de Produto" area="Design / Arquitetura">
                            <div class="info">
                                <span>Guilherme Atan</span>
                                <span>Design de Produto</span>
                                <span>Design / Arquitetura</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-nathalia.jpg)" alt="Nathalia Vobeto" prof="Engenharia Civil" area="Planejamento">
                            <div class="info">
                                <span>Nathalia Vobeto</span>
                                <span>Engenharia Civil</span>
                                <span>Planejamento</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-leticia-silva.jpg)" alt="Letícia da Silva" prof="Engenharia Ambiental" area="Desenvolvimento">
                            <div class="info">
                                <span>Letícia da Silva</span>
                                <span>Engenharia Ambiental</span>
                                <span>Desenvolvimento</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-sayuri.jpg)" alt="Sayuri Matsumoto" prof="Engenharia Ambiental" area="Desenvolvimento">
                            <div class="info">
                                <span>Sayuri Matsumoto</span>
                                <span>Engenharia Ambiental</span>
                                <span>Desenvolvimento</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-tiago.jpg)" alt="Tiago Castelo" prof="Administração de Empresas" area="Comunicação">
                            <div class="info">
                                <span>Tiago Castelo</span>
                                <span>Administração de Empresas</span>
                                <span>Comunicação</span>
                            </div>
                        </div>
                        <div equipe style="background-image:url(./assets/neve-cristiano.jpg)" alt="Cristiano Von Steinkirch" prof="Engenharia Ambiental" area="Desenvolvimento">
                            <div class="info">
                                <span>Cristiano Von Steinkirch</span>
                                <span>Engenharia Ambiental</span>
                                <span>Desenvolvimento</span>
                            </div>
                        </div>
                    </square-gallery>

                    <h1 info class="nome">Francisco Santana</h1>
                    <h2 info class="area">Olhar Interno / Imersão</h2>
                    <h3 info class="prof">Líder comunitário de Milades Do Maranhão</h3>
                </div>
                
            </section>

            <section id="bottomVideo">
                <info-group center>
                    <span slot="major">um banheiro</span>
                    <span slot="pos">pode mudar vidas?</span>
                    <span slot="text">Neve acredita que sim! Veja a transformação de uma das comunidades de Milagres so Maranhão</span>
                </info-group>
                <play-button onclick="window.playVideo('jpoLZjF3j_Y')"></play-button>
            </section>
        `;
        this.staf=this.querySelector("#gallery square-gallery");
        this.staf.addEventListener("selectionChanged", this.stafChanged.bind(this));

        this._bindResize=this.onresize.bind(this);
        this.galleries=Array.from(this.querySelectorAll("square-gallery"));
        window.addEventListener("resize", this._bindResize);

        this.desk=undefined;
        this.onresize();
    }

    onresize(){
        let newdesk=false;

        if(window.innerWidth>768){
            newdesk=true;
        }

        if(this.desk!=newdesk){
            this.desk=newdesk
            if(newdesk){
                this.galleries.forEach(g => {
                    g.setAttribute("disable", "")
                });
                this.setAttribute("desk", "");
            }else{
                this.galleries.forEach(g => {
                    g.removeAttribute("disable", "")
                });
                this.removeAttribute("desk")
            }
            
        }
    }

    disconnectedCallback() {
        window.removeEventListener("resize", this._bindResize);
    }

    stafChanged(e){
        this.querySelector("#gallery .nome").innerHTML=e.detail.attributes.alt;
        this.querySelector("#gallery .area").innerHTML=e.detail.attributes.area;
        this.querySelector("#gallery .prof").innerHTML=e.detail.attributes.prof;
    }

}
customElements.define('home-page', HomePage);