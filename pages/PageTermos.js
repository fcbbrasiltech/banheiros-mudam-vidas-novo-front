export class PageTermos extends HTMLElement {

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>
                page-termos ol *:not(a){
                    margin: 0;
                    margin-bottom: var(--sidePadding);
                }

                page-termos{
                    color: var(--colorBlack);
                    padding: var(--verticalPadding) var(--sidePadding);
                    display:block;
                    overflow-wrap: break-word;
                    font-weight:100;
                    font-size:var(textSizeBody);
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                    background-color:white;
                }

                page-termos h1, page-termos h2, page-termos h3 {
                    color: var(--colorLightBlue);
                }

                page-termos h1{
                    margin: 0;
                    font-size: var(--textSizeSmallerMajor);
                    font-weight:900;
                }

                page-termos h2{
                    font-size:var(--textSizeHeading);
                }

                page-termos h3{
                    font-size:var(--textSizeSmallerHeading);
                }

                page-termos h4{
                    font-size:var(--textSizeSmallerHeading);
                }


                page-termos li{
                    margin-bottom: 1em;
                }

                page-termos ol>li{
                    counter-increment: counter;
                }

                page-termos ul {
                    padding-left:1em;
                    list-style-type: disc;
                    list-style-position: inside;
                }

                page-termos ol{
                    list-style-position: inside;
                    padding-left:1em;
                    list-style: none;
                    counter-reset: counter;
                }

                page-termos li>*:first-child:not(a):before{
                    content: counter(counter) ". ";
                }

                page-termos ol[type="I"] li:before{
                    content: counter(counter, upper-roman) ". ";
                }

                page-termos ol[type="A"]>li>*:first-child:before{
                    content: counter(counter, upper-latin) ". ";
                }

                page-termos ol[all]>li:before{
                    content: counter(counter, upper-latin) ". ";
                }
            </style>

                <h3>Política de Privacidade</h3>
                <p>Este site é propriedade da Kimberly-Clark Brasil Indústria e Comércio de Produtos de Higiene Ltda., subsidiária da Kimberly-Clark Corporation, doravante denominada simplesmente "Kimberly-Clark".</p>
                <p>Aqui, na Kimberly-Clark, nós reconhecemos que muitos visitantes do nosso site têm preocupações com as informações que disponibilizam e como são tratadas por nós. Nossa Política de Privacidade trata dessas preocupações. Esta Política será atualizada periodicamente, então, por favor, visite-a de tempos em tempos para verificar se foi alterada.</p>
                <p><strong>Nossa Política</strong><br>
                    No nosso site, não coletamos informação pessoal, capaz de identificar os visitantes, a menos que eles as forneçam espontaneamente e conscientemente. Você pode visitar nosso site sem nos dizer quem você é ou revelar informações sobre você. No entanto, se você concordar em fornecê-las espontaneamente, por meio de solicitação de cadastramento, nós provavelmente entraremos em contato de tempos em tempos.</p>
                <p>Há algumas seções de nosso site que requerem que você se cadastre para usá-las. E, se você se cadastrar, nós usaremos a informação que você forneceu com o propósito de fornecer-lhe produtos, informação sobre produtos ou serviços que você tenha solicitado. Em razão do volume de solicitações que recebemos, entretanto, nós poderemos passar seu nome, e-mail e endereço postal para um entregador ou distribuidor de produtos que tenha parceria conosco. Nossos parceiros lhe atenderão com informações ou produtos nos quais você tenha demonstrado interesse. Nossos parceiros concordaram ou se comprometeram em manter as informações que eles receberem da Kimberly-Clark em caráter confidencial, a menos que você pessoalmente lhes dê consentimento para divulgarem a outros fabricantes de produtos.</p>
                <p>Nosso site, algumas vezes, inclui notícias de concursos ou outras promoções que estejamos conduzindo e nós poderemos dar permissão para você enviar seus dados eletronicamente, em alguns casos. Se isso ocorrer, nós usaremos as informações que você nos fornecer com o fim de conduzir a promoção. Por exemplo: contatá-lo, se você for o vencedor.</p>
                <p><strong>Crianças</strong><br>
                    A privacidade de crianças é nossa preocupação principal e nossa Política e práticas refletem leis e regulamentos aplicáveis à privacidade das crianças. Nós não coletamos informações pessoais de crianças com menos de 13 anos de idade. Qualquer comunicação que obtivermos, que seja identificada como procedente de crianças com menos de 13 anos não será mantida pela Kimberly-Clark.</p>
                <p><strong>Cookies</strong><br>
                    Como várias companhias, nós, algumas vezes, usamos “cookie” em nosso site. Esses cookies são armazenados em nosso computador por um browser. Quando você se conecta, esse tipo de cookie nos informa se você nos visitou antes ou se é um novo visitante. O cookie não obtém nenhuma informação pessoal sobre você, nem nos fornece nenhum meio de contato com você e o cookie não extrai nenhuma informação do seu computador. Nós usamos o cookie apenas para nos ajudar a identificar o número de suas visitas ao nosso site e em quais áreas você tem maior interesse, de forma que possamos lhe fornecer mais do que você deseja.</p>
                <p><strong>Entre em contato</strong><br>
                    Se você tem alguma questão sobre Política de Privacidade, você poder nos contatar via e-mail (<a href="http://www.kimberly-clark.com.br/novo/contato.aspx" target="_blank">www.kimberly-clark.com.br/novo/contato.aspx</a>) ou telefone: 0800-709- 5599. Para envio de correspondência, utilize o seguinte endereço:</p>
                <p>Kimberly-Clark Brasil Indústria e Comércio de Produtos de Higiene Ltda.<br>
                    Av. Eng. Luís Carlos Berrini, 105, 8º e 9º andares, Itaim Bibi<br>
                    São Paulo – SP<br>
                    CEP: 04.571-900</p>

                <h3>Termos de uso</h3>
                <p>Esse site é propriedade da Kimberly-Clark Brasil Indústria e Comércio de Produtos de Higiene Ltda., subsidiária da Kimberly-Clark Corporation, doravante denominada simplesmente "Kimberly-Clark".</p>
                <p>IMPORTANTE! POR FAVOR, LEIA ESSA DECLARAÇÃO LEGAL CUIDADOSAMENTE ANTES DE USAR ESSE SITE. SE VOCÊ NÃO CONCORDA COM ESSES TERMOS E CONDIÇÕES, NÃO USE ESTE SITE.</p>
                <p>O seu uso deste site e das informações disponíveis neste site estão sujeitos aos seguintes termos e condições:</p><p>
                        </p><p>Posse – este site pertence, é operado e mantido pela Kimberly-Clark Brasil ("Kimberly-Clark") e foi criado para a sua informação e comunicação. Este site e todos os componentes perceptíveis do mesmo, incluindo, sem limitação, os textos, imagens e áudio, são protegidos pelas leis de direitos autorais e pertencem à Kimberly-Clark. </p>
                        <p>Restrições de Uso – este site está disponível somente para seu uso pessoal e não comercial. Você não pode copiar, reproduzir, republicar, divulgar, distribuir, transmitir ou modificar de qualquer maneira a totalidade ou qualquer parte deste site.</p>
                        <p>Notificação de Marca Registrada – todos os nomes, logotipos e marcas registradas são de propriedade da Kimberly-Clark ou a esta licenciados e você não pode usar nenhum desses materiais para qualquer finalidade sem o consentimento expresso por escrito da Kimberly-Clark.</p>
                        <p>Ideias Não Solicitadas – a Kimberly-Clark está ansiosa por receber os seus comentários e responder às suas perguntas sobre os nossos produtos e nossa empresa. Entretanto, não estamos buscando, nem podemos aceitar ideias, sugestões ou materiais não solicitados relativos ao desenvolvimento, projeto, fabricação ou comercialização de nossos produtos. Ao aderir a essa política, esperamos evitar mal-entendidos posteriores entre os membros do público que apresentam comentários ou ideias relativas a produtos ou conceitos desenvolvidos por funcionários da Kimberly-Clark. Se você for contratado pela Kimberly-Clark, nenhum dos materiais fornecidos neste site se constitui ou deve ser considerado como parte ou a totalidade de um contrato de emprego. Além disso, as declarações feitas não constituem garantias ou obrigações por parte da Kimberly-Clark, que mantém um relacionamento voluntário com seus funcionários.</p>
                        <p>Divulgação e Uso de suas Comunicações – mensagens divulgadas neste site e enviadas por correio eletrônico à Kimberly-Clark não são confidenciais e a Kimberly-Clark não poderá ser responsabilizada por qualquer uso ou divulgação das mesmas. Todas as comunicações e outros materiais (inclusive, entre outros, ideias, sugestões ou materiais não solicitados) que você enviar para este site ou para a Kimberly-Clark por meio de correio eletrônico são, e deverão permanecer, de propriedade única e exclusiva da Kimberly-Clark e poderão ser usados pela Kimberly-Clark para qualquer propósito que a mesma desejar, comercial ou qualquer outro, sem remuneração. Se você estiver utilizando este site como um meio de fornecer informações para a Kimberly-Clark, baseadas em um relacionamento fornecedor-comprador, você declara e garante que as especificações de matéria-prima e todas as outras informações que você fornecer são precisas e completas e que você deverá nos notificar imediatamente sempre que houver uma modificação ou outra alteração em quaisquer de tais informações.</p>
                        <p>EXONERAÇÃO DE RESPONSABILIDADE – A KIMBERLY-CLARK NÃO FAZ NENHUMA DECLARAÇÃO OU DÁ QUAISQUER GARANTIAS EM RELAÇÃO À PRECISÃO, CONFIABILIDADE OU INTEGRIDADE DO CONTEÚDO (TEXTOS E IMAGENS) DESTE SITE, NEM TAMPOUCO DÁ QUALQUER GARANTIA DE QUE ESTE SITE OU O SERVIDOR QUE O TORNA DISPONÍVEL ESTÃO LIVRES DE “VÍRUS” DE COMPUTADORES.</p>
                        <p>Limitação de Responsabilidade – a Kimberly-Clark não poderá ser responsabilizada por danos de qualquer tipo, incluindo, sem limitação, danos especiais ou decorrentes de seu acesso a este site, ou incapacidade de acessar ou usar este site, ou ainda, confiança neste site ou no conteúdo do mesmo. Além disso, a Kimberly-Clark não tem nenhuma obrigação de atualizar este site ou o conteúdo do mesmo e a Kimberly-Clark não deverá ser responsabilizada por qualquer falha na atualização de tais informações. Além do mais, a Kimberly-Clark não é responsável nem pode ser responsabilizada pelo seu uso de outros sites da Internet que você possa acessar por meio de determinados links dentro deste site. Estes links e outros “recursos” citados neste site são fornecidos meramente como uma ferramenta para facilitar a navegação dos usuários na Internet e a inclusão em nosso site não se constitui um endosso, recomendação ou afiliação com a Kimberly-Clark.</p>
                        <p>Autorização de uso de imagem – o usuário está ciente de que, ao utilizar este site, concorda em conceder à Kimberly-Clark, em caráter irrevogável e irretratável, a título gratuito, autorização para uso de sua imagem por período indeterminado em todo o território nacional. O usuário autoriza ainda a utilização de sua imagem no todo ou em parte, no site e/ou rede social da Kimberly-Clark e outros meios de publicação/divulgação online que vierem a ser utilizados pela Kimberly-Clark para divulgação de sua campanha denominada “Metade de nós”. O usuário garante que a imagem por ele cedida neste site é de sua propriedade, responsabilizando-se por eventuais reclamações de terceiros, eximindo a Kimberly-Clark de qualquer tipo de responsabilidade.</p>
                        <p>Menores de 18 anos – o usuário garante que possui mais de 18 (dezoito) anos. Caso não possua mais de 18 anos de idade, está representando pelo seu responsável legal.</p>
                        <p>Outros – a Kimberly-Clark, a seu critério exclusivo, se reserva o direito de (1) alterar essa Declaração Legal, (2) monitorar e remover divulgações, e (3) descontinuar a disponibilização deste site a qualquer momento sem aviso prévio. Se qualquer termo, condição ou disposição dessa Declaração Legal for determinado como sendo ilegal, inválido, nulo ou por qualquer razão incapaz de ser aplicado, a validade e capacidade de aplicação dos termos, condições e disposições restantes não deverão ser afetados ou prejudicados de qualquer forma por isso. Essa Declaração Legal constitui o entendimento total entre você e a Kimberly-Clark ao assunto deste instrumento. Ao usar este site, você está reconhecendo o seu consentimento com os termos e condições anteriormente citados.</p>
                        <p>E-mail: (<a href="http://www.kimberly-clark.com.br/novo/contato.aspx" target="_blank">www.kimberly-clark.com.br/novo/contato.aspx</a>) / Telefone: 0800-709 5599</p>
        `;
    }

}
customElements.define('page-termos', PageTermos);