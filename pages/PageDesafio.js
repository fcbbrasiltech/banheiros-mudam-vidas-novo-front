import {InfoGroup} from "../components/InfoGroup.js"
import {SimpleButton} from "../components/SimpleButton.js"


export class PageDesafio extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>

                page-desafio{
                    color: var(--colorBlack);
                }

                page-desafio section{
                    padding: var(--verticalPadding) var(--sidePadding);
                    display:grid;
                    grid-template-columns: 1fr;
                    grid-gap: var(--verticalPadding);
                    background-color:white;
                }

                page-desafio #top{
                    color:white;
                    background-image: url(./assets/eye.jpg);
                    background-size: cover;
                    background-position: center center;
                    font-weight: 100;
                    position:relative;
                    overflow:hidden;
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                }

                page-desafio #top img{
                    height:3em;
                }

                page-desafio #top span{
                    font-size: var(--textSizeBody);
                }

                page-desafio h1{
                    font-weight: 500;
                    font-size: var(--textSizeSmallerMajor);
                    color:var(--colorLightBlue);
                    margin:0;
                    font-weight:900;
                }

                page-desafio #top h2{
                    font-weight: 700;
                    font-size: var(--textSizeMajor);
                    color:white;
                    margin:0;
                }

                page-desafio #saiba-mais{
                    font-size: .8em;
                    background-color:var(--colorDarkBlue);
                    line-height: 2em;
                    padding: 0 .5em;
                    position:absolute;
                    margin-right:2em;
                    position:absolute;
                    bottom:0px;
                    right:var(--sidePadding);
                    cursor:pointer;
                }

                page-desafio #saiba-mais:after{
                    content:"+";
                    width:2em;
                    height:2em;
                    display: block;
                    background-color:var(--colorLightBlue);
                    position:absolute;
                    right:-2em;
                    top:0px;
                    text-align:center;
                    line-height:2em;
                }

                page-desafio h2{
                    color:var(--colorLightBlue);
                    margin:0;
                    margin-bottom: .5em;
                }
                
                info-group [slot=major] span{
                    font-size:2em;
                }

                page-desafio p, ol{
                    font-weight: 100;
                    font-size: var(--textSizeBody);
                    margin:0;
                    line-height:1.5em;
                }

                page-desafio ol{
                    list-style-position: inside;
                    padding-left:0;
                    margin:0;
                }

                page-desafio li{
                    margin-bottom: 1em;
                }

                page-desafio #participe{
                    background-color:var(--colorDarkBlue);
                    color:white:
                }

                page-desafio #participe ol{
                    color:white;
                    font-size: initial;
                    font-weight: 500;
                }



                page-desafio #participe ol p{
                    display:block;
                    font-size: var(--textSizeSource);
                    font-weight: 100;
                    margin: .5em 0 ;
                }

                page-desafio #participe ol p:first-child{
                    margin-top:1em;
                }

                page-desafio a:link, page-desafio a:visited , page-desafio a:hover , page-desafio a:active{
                    color: inherit;
                }
            </style>
            <div>
                <section id="top">
                    <div>
                        <a href="https://marcaneve.com.br/">
                            <img src="./assets/logo-neve-shadow.svg" alt="">
                        </a>
                        <span>Apresenta</span>
                    </div>
                    <h1>Banheiros Mudam Vidas</h1>
                    <h2>O Desafio</h2>

                    <a href="/regulamento">
                        <div id="saiba-mais">Saiba Mais</div>
                    </a>
                </section>

                <section id="dados">
                    <h1>O saneamento básico precisa de cuidados</h1>
                    <info-group>
                        <span slot="major"><span>+4</span> milhões</span>
                        <span slot="text">de brasileiros não têm acesso a banheiro.
                        </span>
                    </info-group>

                    <info-group>
                        <span slot="major"><span>+96</span> milhões</span>
                        <span slot="text">não têm acesso a coleta e tratamento de esgoto.
                        </span>
                    </info-group>

                    <div>
                        <h2>
                            Sua iniciativa pode mudar o saneamento básico.
                        </h2>
                        <p>
                            Metade da população brasileira não possui saneamento e esse problema impacta de forma direta, na educação, saúde e na vida das pessoas. Por isso, Neve está sempre participando de muitos programas para levar soluções relevantes para as mais diversas comunidades brasileiras, como o Banheiros Mudam Vidas. Com esses programas, queremos transformar as comunidades que sofrem com a falta de saneamento básico. Faça parte de mais um programa Neve.
                        </p>
                    </div>

                    <div>
                        <h2>
                            Quer saber como transformar essa realidade?
                        </h2>
                        <ol>
                            <li>Sua iniciativa precisa ter foco na melhoria dos serviços de coleta ou tratamento de esgoto.</li>
                            <li>É preciso se inscrever.</li>
                            <li>De 8 a 10 iniciativas receberão 4 meses de capacitação e mentoria.</li>
                            <li>As 4 melhores iniciativas serão premiadas com 50 mil e serão acompanhadas por mais 6 meses.</li>
                        </ol>
                    </div>
                </section>

                <section id="participe">
                    <h1>Inscrições encerradas</h1>

                    <ol>
                        <li>
                            Inscrição
                            <p>17 de Julho de 2019 até o final do dia 25 de Agosto de 2019.</p>
                        </li>

                        <li>
                            Selecionados para oficina pré-seleção
                            <p>Divulgação dos selecionados para a oficina será feita entre 16 e 30 de Setembro de 2019.</p>
                        </li>
                        
                        <li>
                            Oficina pré-seleção
                            <p>Oficina de Pré-seleção: 08 à 10 de outubro de 2019 contaremos com 12 iniciativas.</p>
                        </li>

                        <li>
                            Programa de aceleração
                            <p>A divulgação dos 8 à 10 selecionados para o Programa de Aceleração será feita até 11 de outubro. E as oficinas serão feitas entre 14/10/2019 à 19/02/2020</p>
                            <p>*Oficina Intermediária: 10 à 12 de dezembro de 2019</p>
                            <p>*Oficina de Encerramento: 18 e 19 de fevereiro de 2020</p>
                            <p style="color:var(--colorLightBlue)">*Teremos webinars e  calls ao longo dos 4 meses do programa.</p>
                        </li>

                        <li>
                            Acompanhamento para as 4 iniciativas premiadas da Aceleração
                            <p>De Março à Agosto de 2020.</p>
                        </li>
                    </ol>

                </section>
            </div>
        `;
    }
    connectedCallback() {
        console.log('Custom square element added to page.');
    }

    disconnectedCallback() {
        console.log('Custom square element removed from page.');
    }

    adoptedCallback() {
        console.log('Custom square element moved to new page.');
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Custom square element attributes changed.');
    }
}
customElements.define('page-desafio', PageDesafio);