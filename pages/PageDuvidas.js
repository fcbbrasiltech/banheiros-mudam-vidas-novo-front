import {ImagedInfoGroup} from "../components/ImagedInfoGroup.js"
import {ExpandInfo} from "../components/ExpandInfo.js"

export class PageDuvidas extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>
                page-duvidas section{
                    padding:var(--verticalPadding) var(--sidePadding);
                    display:grid;
                    grid-template-columns: 1fr;
                    grid-gap: var(--verticalPadding);
                    box-sizing:border-box;
                    min-height: var(--minCoverHeight);
                    align-content:center;
                    background-color:white;
                }

                page-duvidas #duvidas{
                    background-image:url(./assets/bg-doubts.jpg);
                    background-size: cover;
                    background-position: center right;
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                   
                }

                page-duvidas #duvidas info-group{
                    --firstColor:white;
                }

                page-duvidas p, ul{
                    padding:0;
                    margin:0;
                    line-height:1.5em;
                }

                page-duvidas p + p{
                    margin-top: 1em;
                }
            </style>
            <div>
                <section id="duvidas">
                    <info-group bigger>
                        <span slot="pre">dúvidas</span>
                        <span slot="major">frequentes</span>
                        <span slot="text">Se você não encontrar respostas para suas dúvidas nessa página ou quiser saber mais sobre a campanha #MetadeDeNós, nos procure em 0800-7095-599.</span>
                    </info-group>
                </section> 
                <section>
                    <expand-info index="1">
                        <div slot="title">O que é o saneamento básico?</div>
                        <div slot="text">
                            <p>O saneamento básico constitui-se como o conjunto de infraestruturas e medidas adotadas pelo governo a fim de gerar melhores condições de vida para a população. No Brasil, esse conceito está estabelecido pela lei nº 11.445/07, compreendendo o conjunto de serviços estruturais de abastecimento de água, esgotamento sanitário, manejo de resíduos sólidos, limpeza e drenagem de lixo e gestão de águas pluviais urbanas.</p>
                        </div>
                    </expand-info>

                    <expand-info index="2">
                        <div slot="title">O que é o programa Banheiros Mudam Vidas?</div>
                        <div slot="text">
                            <p>Melhorar as condições de higiene e saúde no Brasil por meio do saneamento básico é o objetivo da marca Neve com o programa Banheiros Mudam Vidas. O programa contempla iniciativas que visam colocar o tema saneamento básico em pauta, provocar discussões e gerar melhorias efetivas na saúde e higiene da população. A atuação da marca Neve neste cenário se dá por meio de duas iniciativas principais: a parceria com o UNICEF, na região da Amazônia Legal, e o desenvolvimento de uma solução de banheiro inovadora capaz de atender áreas em que há falta de esgotamento sanitário ou abastecimento de água.</p>
                        </div>
                    </expand-info>

                    <expand-info index="3">
                        <div slot="title">Como a marca Neve está ajudando o saneamento básico no Brasil?</div>
                        <div slot="text">
                            <p>
                                A marca Neve tem como o desenvolvimento de uma solução de banheiro sustentável para áreas carentes de saneamento básico no país, de modo que essa seja uma alternativa mais acessível frente ao alto custo e complexidade dos sistemas tradicionais, e que pudesse ajudar cidadãos de áreas sem acesso ao saneamento básico. A ideia é proporcionar uma boa experiência de banheiro para quem nunca teve acesso, conscientizando sobre a importância do banheiro para uma melhor higiene, saúde e bem estar.
                            </p>
                        </div>
                    </expand-info>

                    <expand-info index="4">
                        <div slot="title">Por que a marca Neve escolheu abraçar a causa do saneamento básico?</div>
                        <div slot="text">
                            <p>A falta de condições de saneamento básico é um problema de saúde, pois aumenta a propagação de doenças e afeta o desenvolvimento social e cognitivo das pessoas, por isso esse é um problema real, sério e urgente. O propósito da marca Neve é levar mais higiene e cuidado para as pessoas por meio de seus produtos, e como marca líder no segmento de papel higiênico sentiu a necessidade de influenciar e atuar de forma positiva em relação às questões sanitárias que estão fortemente ligadas à garantia de uma higiene adequada.</p>
                        </div>
                    </expand-info>

                    <expand-info index="5">
                        <div slot="title">Como é o banheiro sustentável desenvolvido por Neve?</div>
                        <div slot="text">
                            <p>A tecnologia de banheiro escolhida foi o já conhecido modelo bason, que é um banheiro seco compostável. Para este projeto, o modelo em questão foi adaptado para que fosse uma solução de baixo custo (versus a construção de fossas e os sistemas tradicionais), produzida com recursos locais (para garantir o empoderamento local e a sustentabilidade a longo prazo), sem dependência de água e autossustentável (ou seja, que permitisse às comunidades que se apropriassem da solução e a replicassem com mais facilidade). Os dejetos sólidos são coletados numa cabine e, com o uso combinado da serragem (matéria seca) e a incidência constante de sol, após 6 meses de armazenagem essa matéria orgânica se transforma em adubo. Já a urina coletada é transportada por um cano até um círculo de bananeiras que deverá ser construído próximo ao local. O banheiro ainda conta com uma estrutura completa: chuveiro, torneira e pia (em caso de água potável disponível), telhado, iluminação com garrafa pet, porta e janela, e é voltado para áreas rurais, aonde há mais espaço, boa incidência de iluminação solar e possibilidade de plantio do círculo de bananeiras (ou qualquer outra planta que realize o processo de evapotranspiração).</p>
                        </div>
                    </expand-info>

                    <expand-info index="6">
                        <div slot="title">Quem desenvolveu o projeto de banheiro?</div>
                        <div slot="text">
                            <p>Neve selecionou oito jovens brasileiros, sendo sete deles universitários de diferentes regiões do país e um deles morador e representante do município de Milagres do Maranhão, cidade em que foi realizado o piloto deste projeto. Eles contaram com o suporte de mentores e convidados especiais para capacitá-los, entre eles, a Mandalah (consultoria em inovação), Projeto Caracol (consultoria em permacultura e bioconstrução), Aline Matulja (engenheira ambiental) e Acupuntura Urbana (consultoria em mobilização de comunidades). Durante o mês de julho de 2016, esses jovens passaram cinco semanas em uma casa no bairro do Sumaré, em São Paulo, desenvolvendo esse projeto.</p>
                        </div>
                    </expand-info>

                    <expand-info index="7">
                        <div slot="title">Quantos banheiros foram construídos?</div>
                        <div slot="text">
                            <p>Até o momento, foram construídos 13 banheiros na cidade de Milagres do Maranhão, no povoado de Patos. O grande objetivo desse projeto foi entregar uma solução de banheiro de baixo custo e complexidade reduzida para que a comunidade, e outras em situação semelhante, pudesse replicar o projeto com investimento e mão de obra próprios. Além disso, o objetivo desta iniciativa é difundir o conhecimento dessa solução para as populações mais carentes que não tem acesso a qualquer forma de saneamento básico, e para isso, no site existe um manual de construção completo da solução para download (<a href="./downloads/Neve_Banheiro_Sustentavel.pdf" target="_blank">Clique aqui</a>).</p>
                        </div>
                    </expand-info>

                    <expand-info index="8">
                        <div slot="title">O que quer dizer “metade de nós não tem acesso ao saneamento básico”?</div>
                        <div slot="text">
                            <p>Segundo dados do Sistema Nacional de Informação sobre Saneamento 2014 do Ministério das Cidades, 49,84% do esgoto gerado no Brasil não é coletado e é despejado irregularmente. Sendo assim, metade das habitações brasileiras ou, melhor dizendo, #MetadeDeNós não têm acesso a um saneamento básico próprio e adequado.</p>
                        </div>
                    </expand-info>

                    <expand-info index="9">
                        <div slot="title">Esse banheiro é um projeto exclusivo?</div>
                        <div slot="text">
                            <p>O bason já é um modelo de sanitário seco conhecido, que foi criado pelo arquiteto holandês Johan Van. A principal diferença deste modelo para a solução desenvolvida pelos jovens com o apoio da marca Neve e dos mentores, é que esse projeto contempla o uso de materiais locais, tendo como inspiração a permacultura, que utiliza técnicas construtivas e matérias-primas disponíveis localmente, garantindo o empoderamento e a sustentabilidade do projeto a longo prazo.</p>
                            <p>Além disso, o modelo desenvolvido pode ou não depender de água para funcionar. Caso haja acesso a esse recurso, o usuário poderá utilizar a pia e o chuveiro externo à estrutura a fim de aprimorar a experiência do banheiro.</p>
                        </div>
                    </expand-info>

                </section>
            </div>
        `;
        this.questions=this.querySelectorAll("expand-info");

        this.questions.forEach(element => {
            element.addEventListener("disableOthers", this.disableOthers.bind(this));
        });
    }

    disableOthers(e){
        this.questions.forEach(element => {
            if(element!=e.target){
                element.removeAttribute("active");
            }
        });
    }
}
customElements.define('page-duvidas', PageDuvidas);