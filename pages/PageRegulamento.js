import {SimpleButton} from "../components/SimpleButton.js"

export class PageRegulamento extends HTMLElement {

    constructor() {
        super();
        this.innerHTML = /*html*/ `
            <style>
                page-regulamento ol *:not(a){
                    margin: 0;
                    margin-bottom: var(--sidePadding);
                }

                page-regulamento{
                    color: var(--colorBlack);
                    padding: var(--verticalPadding) var(--sidePadding);
                    display:block;
                    overflow-wrap: break-word;
                    font-weight:100;
                    font-size:var(textSizeBody);
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                    background-color:white;
                }

                page-regulamento h1, page-regulamento h2, page-regulamento h3 {
                    color: var(--colorLightBlue);
                }

                page-regulamento h1{
                    margin: 0;
                    font-size: var(--textSizeSmallerMajor);
                    font-weight:900;
                }

                page-regulamento h2{
                    font-size:var(--textSizeHeading);
                }

                page-regulamento h3{
                    font-size:var(--textSizeSmallerHeading);
                }

                page-regulamento h4{
                    font-size:var(--textSizeSmallerHeading);
                }


                page-regulamento li{
                    margin-bottom: 1em;
                }

                page-regulamento ol>li{
                    counter-increment: counter;
                }

                page-regulamento ul {
                    padding-left:1em;
                    list-style-type: disc;
                    list-style-position: inside;
                }

                page-regulamento ol{
                    list-style-position: inside;
                    padding-left:1em;
                    list-style: none;
                    counter-reset: counter;
                }

                .alpha-indicador {
                    list-style: upper-alpha;
                }

                page-regulamento li>*:first-child:not(a):before{
                    content: counter(counter) ". ";
                }

                page-regulamento ol[type="I"] li:before{
                    content: counter(counter, upper-roman) ". ";
                }

                page-regulamento ol[type="A"]>li>*:first-child:before{
                    content: counter(counter, upper-latin) ". ";
                }

                page-regulamento ol[all]>li:before{
                    content: counter(counter, upper-latin) ". ";
                }




            </style>

            <h1>Regulamento do Programa de Aceleração do Desafio Neve</h1>
            <ol>
                <li>
                    <h2>O PROGRAMA</h2>
                    <p>A marca Neve®, cuja titularidade é do grupo KIMBERLY-CLARK (doravante denominado Organizador), líder em valor no mercado brasileiro de papel higiênico, acredita que o acesso ao saneamento é uma condição básica que impacta de forma direta na educação, na saúde e na vida das pessoas. Por isso, deve ser um direito de todos</p>
                    <p>Com esse posicionamento, e no intuito de contribuir para que essa realidade seja alcançada, Neve® está lançando, em 2019, a 1ª edição do Programa de Aceleração de iniciativas inovadoras no setor de saneamento. Tal programa tem como objetivo apoiar iniciativas existentes que atuam na melhoria da questão de esgotamento sanitário no Brasil, com vistas a fortalecer tanto o impacto positivo quanto a sustentabilidade financeira de suas operações.</p>
                    <p>O programa será conduzido com momentos presenciais e virtuais, e terá o Sense-Lab como organização responsável pela execução técnica.</p>
                    <p>O foco prioritário é o apoio a projetos e negócios que ofereçam soluções inovadoras para ao menos um dos desafios do programa e tenham potencial de gerar impacto socioambiental nas mais diversas comunidades brasileiras.</p>
                    <p>
                        São considerados desafios do programa:
                        <ol type="I">
                            <li>Sensibilizar e mobilizar a população quanto à importância da coleta de esgoto;</li>
                            <li>Implementar tecnologias inovadoras de coleta e tratamento de esgoto doméstico;</li>
                            <li>Promover o tema como pauta prioritária na agenda política.</li>
                        </ol>
                    </p>

                    <h3>O programa será composto pelas seguintes etapas:</h3>
                    <ol type="A">
                        <li>
                            <h4>Programa de Aceleração</h4>
                            <ul>
                                <li>Consiste no processo de capacitação e mentoria que visa fortalecer e estruturar iniciativas que trabalhem na melhoria e expansão da oferta de serviços de esgoto no Brasil;</li>
                                <li>O programa será composto por um conjunto de interações em oficinas presenciais e virtuais entre os Participantes e o Executor Técnico, com o apoio do Organizador. As oficinas presenciais acontecerão em São Paulo nas seguintes datas: 1) Oficina de pré-seleção das iniciativas: 8, 9 e 10 de outubro de 2019; 2) Oficina intermediária: 10, 11 e 12 de dezembro de 2019; 3) Oficina de encerramento: 18 e 19 de fevereiro de 2020. As oficinas virtuais são compostas de webinars, veiculados via Zoom ou plataforma similar, cujo link será enviado por e-mail ao Participante, com datas a serem divulgadas no início do Programa. Intercalados com os webinars, os participantes terão calls individuais com o Executor Técnico para tratar de necessidades específicas de suas iniciativas;</li>
                                <li>Esta etapa será composta pela disponibilização de conteúdos via webinar, que serão fornecidos pelo Executor Técnico aos Participantes durante as oficinas presenciais e virtuais: Modelo C – uma abordagem integrada para modelar negócios de impacto; Modelos de Receita; Teoria de Mudança e Vetores de Impacto; Design Evolutivo – uma abordagem para gerar inovação e negócios de impacto socioambiental; Curvas de Valor e Processos de Inovação Social; QEMP – Quociente Empreendedor; Lean Startup, Effectuation, Prototipagem; Ferramentas para Planejamento de Negócios; entre outras;</li>
                                <li>Esta etapa terá a duração total de aproximadamente 130 dias, de 8 de outubro de 2019 até 19 de fevereiro de 2020.</li>
                            </ul>
                        </li>

                        <li>
                            <h4>Premiação e Acompanhamento das iniciativas escolhidas na etapa anterior</h4>
                            <ul>
                                <li>Ao final da etapa do Programa de Aceleração e após a avaliação de uma banca de profissionais do setor, até 4 (quatro) Iniciativas-Participantes serão escolhidas e premiadas com um capital-semente de R$ 50.000,00 (cinquenta mil reais), e seguirão para a etapa de Acompanhamento com o Executor Técnico, sob a supervisão da empresa Organizadora;</li>
                                <li>O pagamento da premiação será realizado em conta-corrente indicada pelo Escolhido, em 30 dias após a escolha de sua iniciativa, sendo de inteira responsabilidade do Escolhido a correta indicação dos dados bancários, de modo que qualquer problema quanto ao pagamento derivado de falha ou erro nas informações dos dados não representará nenhuma responsabilidade ao Organizador;</li>
                                <li>A seleção dos membros da banca de profissionais responsáveis pela escolha das iniciativas que participarão da etapa de Acompanhamento será realizada a critério exclusivo e discricionário do Organizador, sem nenhuma possibilidade de contestação ou interferência dos Participantes/Escolhidos;</li>
                                <li>A seleção dos Escolhidos será feita por meio de uma banca de profissionais indicados pelo Organizador, levando-se em consideração especialmente, mas não exclusivamente, a evolução da Iniciativa-Participante ao longo da etapa do Programa de Aceleração e o seu potencial em se tornar um negócio economicamente autossustentável, de acordo com os critérios exclusivamente estabelecidos pela Organizadora, nos termos do presente regulamento;</li>
                                <li>A escolha da banca será realizada a exclusivo e discricionário critério do Organizador, sendo incontestável e inimpugnável por quaisquer dos Participantes/Escolhidos, ainda que não leve em consideração os parâmetros acima fixados;</li>
                                <li>Além do prêmio financeiro, as iniciativas escolhidas receberão do Executor Técnico, sob a supervisão da empresa Organizadora, um acompanhamento individualizado adicional para implementação das estratégias desenvolvidas ao longo da etapa do Programa de Aceleração;</li>
                                <li>Os conteúdos que serão fornecidos para implementação das estratégias de desenvolvimento serão estabelecidos e fornecidos levando-se em consideração a particularidade e necessidades de cada iniciativa;</li>
                                <li>O formato deste desenvolvimento e implementação estratégica será por meio de conferências telefônicas e eventuais reuniões presenciais;</li>
                                <li>Esta etapa tem a duração de 6 (seis) meses, de março a agosto de 2020.</li>
                            </ul>
                        </li>
                    </ol>

                    <h3>Perfil desejado dos Participantes</h3>
                    <p>Incluem-se como público-alvo deste Programa startups, projetos acadêmicos ou de atividades de entidades sem fins lucrativos. Independentemente da sua constituição jurídica, as iniciativas devem:</p>
                    <ol type="A" class="alpha-indicador">
                        <li>Ter suas soluções/iniciativas (produtos e serviços) já desenvolvidas e testadas, no mínimo, via programas pilotos;</li>
                        <li>Ter foco direto ou indireto na promoção da melhoria da questão de esgotamento sanitário no país;</li>
                        <li>Ter objetivo de gerar impacto (focar na população menos atendida pelas soluções atuais) via um modelo financeiramente sustentável</li>
                    </ol>

                    <h3>Custos para os Participantes</h3>
                    <p>A inscrição no programa é gratuita para todos os interessados. Da mesma forma, toda as etapas subsequentes (oficina de estruturação das iniciativas a partir da metodologia-base, Programa de Aceleração e Acompanhamento dos Premiados) terão os custos arcados pelo Organizador.</p>
                </li>

                
                <li>
                    <h2>CRITÉRIOS DE ELEGIBILIDADE</h2>
                    <h4>Para ser elegível a participar no Programa, os Candidatos devem atender aos seguintes requisitos:</h4>
                    <ol type="A" all>
                        <li>Ter sua solução (produto ou serviço) desenvolvida e testada. O programa busca trabalhar com iniciativas que já passaram pela fase de ideação da solução;</li>
                        <li>O produto ou serviço ofertado deverá ter foco na melhoria da questão de esgotamento sanitário no país. Especial atenção será dada para iniciativas que levem suas soluções para comunidades isoladas – núcleos habitacionais que não estão conectados aos serviços públicos e estão privados de saneamento básico;</li>
                        <li>Equipe constituída de, pelo menos, 2 membros, comprovando-se a sua relação com a empresa, instituição ou organização participante;</li>
                        <li>No mínimo um sócio com dedicação integral ao programa e a quem todas as informações e mensagens do Organizador serão dirigidas;</li>
                        <li>A iniciativa deve ter a intenção de gerar receita própria ou captar investimento financeiro. Não é requerido que a iniciativa possua CNPJ de empresa no momento da inscrição;</li>
                        <li>Ter comprometimento e disponibilidade de participar em todas as atividades, tanto as virtuais quanto as presenciais.</li>
                    </ol>
                </li>

                <li>
                    <h2>PROCESSO DE INSCRIÇÃO</h2>
                    <ul>
                        <li>Todos os candidatos interessados em participar do programa devem preencher o formulário de inscrição disponível no site http://www.banheirosmudamvidas.com.br;</li>
                        <li>O período de inscrições inicia-se em 17 de julho de 2019 e encerra-se às 23h59min (horário de Brasília) do dia 25 de agosto de 2019;</li>
                        <li>Em caso de alteração nas datas acima mencionadas, as novas datas serão comunicadas pelo website http://www.banheirosmudamvidas.com.br.</li>
                    </ul>
                </li>


                <li>
                    <h2>SELEÇÃO DOS PARTICIPANTES</h2>
                    <ul>
                        <li>Somente formulários completos e submetidos via site oficial serão aceitos para participar do processo seletivo do Programa;</li>
                        <li>Do total de inscritos até a data limite, 12 (doze) iniciativas serão pré-selecionadas para participar presencialmente de uma oficina em São Paulo, nos dias 8, 9 e 10 de outubro de 2019;</li>
                        <li>A presença nessa oficina não garante a posterior participação da Iniciativa-Participante no Programa de Aceleração, sendo esse encontro presencial a última etapa do processo seletivo. A comunicação aos Participantes para a oficina será feita entre 16 e 30 de setembro de 2019;</li>
                        <li>Os critérios de seleção para participação na oficina serão: maturidade da iniciativa (estrutura e dedicação da equipe, resultados prévios, protótipos e pilotos executados, entre outros) e capacidade de entrega dos resultados – impacto e financeiro (modelo de negócios, propósito e proposta de valor, entre outros);</li>
                        <li>Após a avaliação dos resultados gerados na oficina em São Paulo, de 8 (oito) a 10 (dez) Iniciativas-Participantes serão oficialmente selecionadas para participar dos 4 (quatro) meses do Programa de Aceleração. A comunicação dos Participantes escolhidos para o programa será feita até o fim do dia 11 de outubro de 2019;</li>
                        <li>Os critérios de seleção para o Programa de Aceleração serão os mesmos da pré-seleção, no entanto com maior nível de aprofundamento pelas interações geradas na oficina;</li>
                        <li>Na oficina de encerramento do Programa de Aceleração, que acontecerá nos dias 18 e 19 de fevereiro de 2020, as iniciativas premiadas com o capital-semente – e que seguirão para a fase de Acompanhamento dos Premiados – serão comunicadas após a realização do pitch final;</li>
                        <li>A escolha destas iniciativas será feita por meio de uma banca de profissionais indicados pelo Organizador, com liberdade para escolher até (4) Iniciativas-Participantes, levando-se em consideração a evolução da iniciativa ao longo da etapa do Programa de Aceleração e seu potencial para se tornar um negócio economicamente autossustentável;</li>
                        <li>A escolha dos membros da banca será realizada ao exclusivo e discricionário critério do Organizador, sendo incontestável e inimpugnável por qualquer dos Participantes, optando-se preferencialmente por profissionais acadêmicos, potenciais investidores, empreendedores sociais e representantes de incubadores, atentando-se para a disponibilidade desses referidos profissionais.</li>
                    </ul>
                </li>

                <li>
                    <h2>FORMAS DE COMUNICAÇÃO</h2>
                    <ul>
                        <li>Todas as informações de interesse geral do público serão disponibilizadas no website http://www.banheirosmudamvidas.com.br;</li>
                        <li>Durante o período de inscrição, havendo qualquer dúvida ou dificuldade no preenchimento/submissão do formulário, o Candidato poderá enviar um e-mail para a equipe do Sense-Lab – organização responsável pela coordenação e execução do Programa de Aceleração. O endereço de contato é desafioneve@sense-lab.com;</li>
                        <li>Em todas as etapas posteriores (oficina de estruturação, Programa de Aceleração e Acompanhamento dos Premiados), a comunicação oficial será via e-mail indicado pelo Participante no formulário de inscrição;</li>
                        <li>O preenchimento do e-mail para contato é de exclusiva responsabilidade do Candidato/Participante, e qualquer erro ou problema de recebimento de mensagens eletrônicas não poderá ser imputado ao Organizador.</li>
                    </ul>
                </li>

                <li>
                    <h2>CONFIDENCIALIDADE</h2>
                    <ul>
                    <li>O Programa respeita a confidencialidade das informações das Iniciativas-Participantes e se compromete a usá-las apenas dentro do necessário para execução e divulgação do Programa. Nenhuma informação será disponibilizada em comunicação pública sem o prévio consentimento e autorização dos responsáveis pelas iniciativas;</li>
                    <li>Os Participantes obrigam-se a manter o mais absoluto sigilo com relação a quaisquer dados, informações, materiais, produtos, sistemas, técnicas, estratégias, métodos de operação, conteúdos, pormenores, inovações, segredos comerciais, marcas, criações, especificações técnicas e comerciais, entre outros, doravante denominados DADOS CONFIDENCIAIS, a que eles ou qualquer de seus parceiros comerciais, diretores, funcionários e/ou prepostos venham a ter acesso, conhecimento, ou que venham a lhes ser confiados em razão da participação no referente Programa, comprometendo-se, outrossim, a não revelar, reproduzir, utilizar ou dar conhecimento a terceiros, em hipótese alguma, bem como a não permitir que nenhum de seus diretores, funcionários e/ou prepostos faça uso indevido de DADOS CONFIDENCIAIS;</li>
                    <li>Os Participantes se comprometem a tratar com confidencialidade todo o conteúdo fornecido durante as etapas do programa, incluindo informações de iniciativas dos demais participantes.</li>
                    </ul>
                </li>

                <li>
                    <h2>DOS DIREITOS DE PROPRIEDADE INTELECTUAL</h2>
                    <ul>
                        <li>Ao longo de todas as etapas do Programa, o Organizador fará comunicações públicas periódicas no intuito de divulgar e dar visibilidade tanto ao Programa quanto às Iniciativas-Participantes;</li>
                        <li>O Participante/Escolhido concorda em estar disponível para o relacionamento com a agência de comunicação indicada pelo Organizador, seja cedendo entrevistas ou se disponibilizando para sessões de fotos e filmagem que eventualmente sejam requisitadas e autoriza o Organizador ou quem este autorizar a utilizar o seu nome, imagem e voz em todo e qualquer meio para atender os objetivos do Programa, especialmente o uso do seu nome, imagem e voz no material de suporte e divulgação eventualmente elaborado pelo Organizador, seja durante o período do Programa ou mesmo após o seu término;</li>
                        <li>O Participante/Escolhido reconhece, ainda, que toda a propriedade intelectual decorrente de eventual elaboração de material de divulgação do Programa é de exclusiva propriedade do Organizador;</li>
                        <li>O Participante/Escolhido assume a integral responsabilidade por todas e quaisquer demandas relativas à violação de direitos de propriedade intelectual de terceiros relativamente às Iniciativas-Participantes e materiais apresentados, afastando-se qualquer responsabilidade do Organizador neste sentido, incluindo-se, mas não se limitando, a demandas por violação de patentes, desenho industrial, direito autoral, marcas registradas ou segredo de negócio.</li>
                    </ul>
                </li>

                <li>
                    <h2>NATUREZA DO RELACIONAMENTO</h2>
                    <ul>
                        <li>As relações provenientes da participação no Programa não constituem relações ou obrigações trabalhistas, previdenciárias, sociais, ou quaisquer outras, entre o Organizador e os empregados prepostos e contratados do Participante/Escolhido;</li>
                        <li>A participação no Programa não constitui ou configura consórcio ou subcontratação, entre Organizador e Participante/Escolhido ou perante terceiros.</li>
                    </ul>
                </li>

                <li>
                    <h2>PENALIDADES</h2>
                    <ul>
                        <li>O caso de conhecimento de que alguma informação apresentada no formulário de inscrição seja falsa poderá acarretar a automática exclusão do Candidato/Participante/Escolhido em qualquer fase;</li>
                        <li>A decisão caberá ao Organizador de maneira exclusiva;</li>
                        <li>Além disso, o Participante/Escolhido que não mostrar dedicação e engajamento – seja comparecendo às atividades virtuais e presenciais, seja entregando tarefas dentro dos prazos, seja retornando mensagens da organização – também poderão ter, a critério do Organizador, sua participação cancelada;</li>
                        <li>A alegação ou ajuizamento de medidas judiciais e/ou extrajudiciais por terceiros, relacionadas a infração de direitos relativos à propriedade industrial ou de propriedade intelectual, nos termos da Cláusula 7.4, determinam a imediata exclusão do Participante/Escolhido do Programa, sem prejuízo de indenização por perdas e danos, lucros cessantes ao Organizador.</li>
                    </ul>
                </li>

                <li>
                    <h2>DISPOSIÇÕES GERAIS</h2>
                    <ul>
                        <li>Ao submeter sua inscrição, o Participante atesta que tomou conhecimento e que concorda com o conteúdo deste regulamento;</li>
                        <li>A organização será responsável por conectar mentores externos aos Participantes. Os acordos do posterior relacionamento entre essas duas partes devem ser feitos diretamente entre si, cabendo à organização apenas receber reporte do andamento das interações;</li>
                        <li>O Programa e seu Organizador não participam nem tampouco se responsabilizam por eventuais rodadas de investimento que possam decorrer após o programa;</li>
                        <li>O participante assumirá a integral responsabilidade por todas e quaisquer demandas relativas à violação de direitos de terceiros, relativamente a Propriedade Intelectual da qual não seja detentor, incluindo-se, mas não se limitando, a demandas por violação de patentes, desenho industrial, direito autoral, marcas registradas ou segredo de negócio;</li>
                        <li>As Iniciativas-Participantes escolhidas para a etapa de Premiação e Acompanhamento não poderão ser ofertadas para qualquer empresa concorrente do Organizador por 3 (três) anos após o encerramento deste Programa.</li>
                    </ul>
                </li>
            </ol>

        `;
    }

}
customElements.define('page-regulamento', PageRegulamento);