import {InfoGroup} from "../components/InfoGroup.js"
import {ImagedInfoGroup} from "../components/ImagedInfoGroup.js"
import {SimpleButton} from "../components/SimpleButton.js"


export class PageBanheiros extends HTMLElement{

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>
                page-banheiros section{
                    padding: var(--verticalPadding) var(--sidePadding);
                    display: grid;
                    grid-template-columns: 1fr;
                    grid-gap: var(--verticalPadding);
                    grid-auto-rows: auto;
                    box-sizing: border-box;
                    
                    align-content: space-around;
                    background-color:white;
                    
                }

                page-banheiros p{
                    margin:0px;
                }

                

                page-banheiros section > img{
                    max-width: 100%;
                }

                page-banheiros section p b{
                    font-weight: 500;
                }

                page-banheiros #direito{
                    min-height: var(--minCoverHeight);
                    background-image: url(./assets/bg-takes-care.jpg);
                    background-size: cover;
                    background-position: center center;
                    align-content: center;
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                }

                page-banheiros #direito info-group{
                    font-size: 1.2em;
                    --firstColor:white;
                }

                page-banheiros #infos{
                    padding-left: 0;
                    padding-right: 0;
                }



            </style>

            <section id="direito">
                <info-group bigger center>
                    <span slot="pre">por que todos têm</span>
                    <span slot="major">direito</span>
                    <span slot="pos">ao saneamento básico</span>
                    <span slot="text">
                    Melhorar as condições de higiene e saúde no Brasil por meio do saneamento básico: esse é o objetivo de Neve com o programa <i>Banheiros Mudam Vidas</i>.
                        <br><br>
                        O acesso ao saneamento é uma condição básica que impacta de forma direta na educação, na saúde e na vida das pessoas. Por isso, deve ser um direito de todos.
                    </span>
                </info-group>
            </section>

            <section id="infos">
                <div>
                    <imaged-info-group >
                        <reference-image src="./assets/img-bason-mobile.png"
                            label="UNICEF/BRZ/RAONI LIBÓRIO">
                        </reference-image>
                        <info-group>
                            <span slot="pre">O DESAFIO DE</span>
                            <span slot="major">Neve</span>
                            <span slot="text">
                                Em paralelo à parceria, Neve abraçou o desafio de propor uma solução escalável para a falta de saneamento básico. Milagres do Maranhão, que tem um dos piores índices de saneamento do país, foi a primeira região a receber o modelo de banheiro seco desenvolvido por Neve.
                            </span>
                        </info-group>
                    </imaged-info-group>
                    <simple-button href="/desafio-neve">Conheça o Projeto</simple-button>
                </div>
            </section>

        `;

    }

}
customElements.define('page-banheiros', PageBanheiros);