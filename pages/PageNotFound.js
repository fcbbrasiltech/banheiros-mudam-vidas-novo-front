import {ImagedInfoGroup} from "../components/ImagedInfoGroup.js"
import {ExpandInfo} from "../components/ExpandInfo.js"
import {CustomDropdown} from "../components/CustomDropdown.js"

export class PageNotFound extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>
                page-not-found section{
                    background-color:white;
                    min-height:50vh;
                    padding:var(--verticalPadding) var(--sidePadding);
                    display:grid;
                    padding-top:calc(var(--verticalPadding) + var(--menuHeight));
                    align-content: center;
                    justify-content: center;
                    align-items: center;
                    color: var(--colorLightBlue);
                    font-weight:900;
                    font-size: var(--textSizeMajor);
                    text-align:center;
                }

                page-not-found h1{
                    font-size:var(--textSizeHeading);
                }
            </style>
            <div>
                <section>
                    404
                    <br>
                    <h1>Não Encontrado</h1>
                </section>
            </div>
        `;
    }

}
customElements.define('page-not-found', PageNotFound);