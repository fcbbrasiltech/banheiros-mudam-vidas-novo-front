import {ImagedInfoGroup} from "../components/ImagedInfoGroup.js"
import {ExpandInfo} from "../components/ExpandInfo.js"
import {CustomDropdown} from "../components/CustomDropdown.js"

export class PageStartups extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>
                page-startups{
                    color: var(--colorBlack);
                    --sidePadding:var(--verticalPadding);
                }

                page-startups section{
                    padding: var(--verticalPadding) var(--sidePadding);
                    display:grid;
                    grid-template-columns: 1fr;
                    grid-gap: var(--verticalPadding);
                    background-color:white;
                }

                page-startups #top{
                    color:white;
                    font-weight: 100;
                    position:relative;
                    overflow:hidden;
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                    padding-bottom:calc(var(--verticalPadding) * 2);
                }

                page-startups #top img{
                    height:3em;
                }

                page-startups #top span{
                    font-size: var(--textSizeBody);
                }

                page-startups h1{
                    font-weight: 500;
                    font-size: var(--textSizeSmallerMajor);
                    color:var(--colorLightBlue);
                    margin:0;
                    font-weight:900;
                }

                page-startups #top h2{
                    font-weight: 700;
                    font-size: var(--textSizeMajor);
                    color:white;
                    margin:0;
                }

                page-startups #saiba-mais{
                    font-size: .8em;
                    background-color:var(--colorDarkBlue);
                    line-height: 2em;
                    padding: 0 .5em;
                    position:absolute;
                    margin-right:2em;
                    position:absolute;
                    bottom:0px;
                    right:var(--sidePadding);
                    cursor:pointer;
                }

                page-startups #saiba-mais:after{
                    content:"+";
                    width:2em;
                    height:2em;
                    display: block;
                    background-color:var(--colorLightBlue);
                    position:absolute;
                    right:-2em;
                    top:0px;
                    text-align:center;
                    line-height:2em;
                }

                page-startups h2{
                    color:var(--colorLightBlue);
                    margin:0;
                    margin-bottom: .5em;
                }
                
                info-group [slot=major] span{
                    font-size:2em;
                }

                page-startups div[sidePhoto]{
                    display:grid;
                    grid-gap:var(--verticalPadding);
                    grid-template-columns: auto 60%;
                    grid-template-rows: min-content;
                    position:relative;
                }

                page-startups div[sidePhoto] > div{
                    position:relavive;
                    background-size: cover;
                    background-position:center center;
                }

                page-startups div[product]{
                    display:grid;
                    grid-gap:var(--verticalPadding);
                    grid-template-columns: auto 65%;
                    grid-template-rows: 1fr 1.5fr;
                    position:relative;
                }

                page-startups div[product] simple-button{
                    grid-column: 1 / span 1;
                    align-self:center;
                }

                page-startups div[product] p{
                    grid-column: 1 / span 1;
                    
                }
                page-startups div[product] div{
                    grid-column: 2 / span 1;
                    grid-row: 1 / span 2;
                    position: relative;
                }

                page-startups div[product] img{
                    position: absolute;
                    top:0;
                    right:0;
                    height:100%;
                }

                page-startups #logos{
                    display: flex;
                    justify-content: space-around;
                }


                page-startups p, ol{
                    font-weight: 100;
                    font-size: var(--textSizeBody);
                    margin:0;
                    line-height:1.5em;
                }

                page-startups ol{
                    list-style-position: inside;
                    padding-left:0;
                    margin:0;
                }

                page-startups li{
                    margin-bottom: 2em;
                }

                page-startups #participe{
                    background-color:var(--colorDarkBlue);
                    color:white:
                }

                page-startups #participe a {
                    position: relative;
                    grid-template-columns: 1fr 2em;
                }

                page-startups #participe a:after {
                    content:'';
                    width:2em;
                    height: 1em;
                    position: relative;
                    background-image: url(/assets/pdf-download.svg);
                    background-position: right top;
                    background-size: contain;
                    background-repeat:no-repeat;
                    display: inline-block;
                    margin-left: .1em;
                }

                page-startups #participe ol{
                    column-count: 2;
                    column-gap: var(--verticalPadding);
                    color:white;
                    font-size: initial;
                    font-weight: 500;
                }



                page-startups #participe ol p{
                    display:block;
                    font-size: var(--textSizeSource);
                    font-weight: 100;
                    margin: .5em 0 ;
                }
                page-startups #participe [break]{
                    break-after: column;
                    margin-bottom: 0;
                }

                page-startups #participe ol p:first-child{
                    margin-top:1em;
                }

                page-startups a:link, page-startups a:visited , page-startups a:hover , page-startups a:active{
                    color: inherit;
                }

                page-startups .people{
                    display: grid;
                    grid-gap: var(--sidePadding);
                    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
                }
               
            </style>
            
            <div id="top">
                <section id="participe">
                    <h1>Conheça as iniciativas selecionadas</h1>

                    <ol>
                        <li>
                            <a href='/assets/PDFs/Sapiencia Ambiental.pdf' target="_blank">Sapiência Ambiental</a>
                            <p>Integraremos engenharia, permacultura e educação nos sistemas de saneamento no extremo sul de SP</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Gota.pdf' target="_blank">Gota</a>
                            <p>Estação Individual para Tratamento de Esgotos de Alta Eficiência fabricada em Plástico Reaproveitado</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Sana.pdf' target="_blank">SANA - Soluções de Saneamento Comunitário</a>
                            <p>Sistemas ecológicos que fecham o ciclo - do saneamento à produção de alimentos.</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Taboa Engenharia.pdf' target="_blank">Taboa Engenharia</a>
                            <p>Trabalhamos para contribuir com a universalização do tratamento de esgoto através das tecnologias sociais de saneamento ecológico.</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Biosaneamento.pdf' target="_blank">Biosaneamento</a>
                            <p>Contribuir para a universalização do saneamento básico no Brasil, através do trabalho em comunidades precárias e do engajamento de diferentes agentes da sociedade.</p>
                        </li>
                        <li >
                            <a href='/assets/PDFs/Sist. Trat. Esgoto e Reuso Agricola Familiar.pdf' target="_blank">Sistema de Tratamento de Esgoto e de Reúso Agrícola Familiar</a>
                            <p>O Sistema tem por objetivo prover as populações rurais difusas do Semiárido de infraestrutura de coleta e tratamento de esgoto e, ofertando água de qualidade para produção agrícola.</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Banheiros Biodigestores Ambiencias Possiveis.pdf' target="_blank">Banheiros Biodigestores: Ambiências Possíveis</a>
                            <p>O TETO busca superar a situação de pobreza em que vivem milhares de famílias através do desenvolvimento de projetos possibilitando a transformação do espaço pelos próprios moradores.</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Banheiros pelo Futuro.pdf' target="_blank">BioMovement - HomeBiogas BioToilet</a>
                            <p>O sistema HomeBiogas Bio-Toilet utiliza um vaso de baixa vazão conectado ao biodigestor.</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/Fossa Septica Biodigestora.pdf' target="_blank">Fossa Séptica Biodigestora</a>
                            <p>A Fossa Séptica Biodigestora é formada por um conjunto de, no mínimo, 3 caixas d'água de fibra de vidro de 1000 litros conectadas por tubulações que compõem a tecnologia de tratamento do esgoto doméstico de uma residência de até 5 pessoas.</p>
                        </li>
                        <li>
                            <a href='/assets/PDFs/10Envolver e Saneamento Basico Rural.pdf' target="_blank">10Envolver e Saneamento Básico Rural</a>
                            <p>Esgotamento Sanitário Rural e Participação Popular nos vales do Mucuri e Jequitinhonha - MG</p>
                        </li>
                    </ol>
                </section>
                <br>
                <!--
                <br><br><br><br>
                <h1 style="text-align:center;">Quem está falando da gente:</h1>
                <br><br><br>
                <div id="logos">
                    <img src="./assets/media-news.png" alt="">
                    <img src="./assets/media-globo.png" alt="">
                    <img src="./assets/media-sbt.png" alt="">
                    <img src="./assets/media-record.png" alt="">
                </div>
                -->
            </div>
        `;
    }

}
customElements.define('page-startups', PageStartups);