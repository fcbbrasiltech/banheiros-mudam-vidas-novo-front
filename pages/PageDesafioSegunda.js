import {InfoGroup} from "../components/InfoGroup.js"
import {PhotoCard} from "../components/PhotoCard.js"
import {SimpleButton} from "../components/SimpleButton.js"


export class PageDesafioSegunda extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        this.innerHTML = /*html*/`
            <style>

                page-desafio-segunda{
                    color: var(--colorBlack);
                    --sidePadding:var(--verticalPadding);
                }

                page-desafio-segunda section{
                    padding: var(--verticalPadding) var(--sidePadding);
                    display:grid;
                    grid-template-columns: 1fr;
                    grid-gap: var(--verticalPadding);
                    background-color:white;
                }

                page-desafio-segunda #top{
                    color:white;
                    background-image: url(./assets/eye.jpg);
                    background-size: cover;
                    background-position: center center;
                    font-weight: 100;
                    position:relative;
                    overflow:hidden;
                    padding-top: calc(var(--verticalPadding) + var(--menuHeight));
                }

                page-desafio-segunda #top img{
                    height:3em;
                }

                page-desafio-segunda #top span{
                    font-size: var(--textSizeBody);
                }

                page-desafio-segunda h1{
                    font-weight: 500;
                    font-size: var(--textSizeSmallerMajor);
                    color:var(--colorLightBlue);
                    margin:0;
                    font-weight:900;
                }

                page-desafio-segunda #top h2{
                    font-weight: 700;
                    font-size: var(--textSizeMajor);
                    color:white;
                    margin:0;
                }

                page-desafio-segunda #saiba-mais{
                    font-size: .8em;
                    background-color:var(--colorDarkBlue);
                    line-height: 2em;
                    padding: 0 .5em;
                    position:absolute;
                    margin-right:2em;
                    position:absolute;
                    bottom:0px;
                    right:var(--sidePadding);
                    cursor:pointer;
                }

                page-desafio-segunda #saiba-mais:after{
                    content:"+";
                    width:2em;
                    height:2em;
                    display: block;
                    background-color:var(--colorLightBlue);
                    position:absolute;
                    right:-2em;
                    top:0px;
                    text-align:center;
                    line-height:2em;
                }

                page-desafio-segunda h2{
                    color:var(--colorLightBlue);
                    margin:0;
                    margin-bottom: .5em;
                }
                
                info-group [slot=major] span{
                    font-size:2em;
                }

                page-desafio-segunda div[sidePhoto]{
                    display:grid;
                    grid-gap:var(--verticalPadding);
                    grid-template-columns: 1fr;
                    grid-template-rows: 100vw min-content;
                    position:relative;
                }

                @media (min-width: 768px) {
                    page-desafio-segunda div[sidePhoto]{
                        grid-template-columns: auto 60%;
                        grid-template-rows: min-content;
                    }
                }

                page-desafio-segunda div[sidePhoto] > div{
                    position:relavive;
                    background-size: cover;
                    background-position:center center;
                }

                page-desafio-segunda div[product]{
                    display:grid;
                    grid-template-columns: auto 65%;
                    grid-template-rows: 1fr 1.5fr;
                    position:relative;
                }

                page-desafio-segunda div[product] simple-button{
                    grid-column: 1 / span 1;
                    align-self:center;
                }

                page-desafio-segunda div[product] p{
                    grid-column: 1 / span 1;
                    
                }
                page-desafio-segunda div[product] div{
                    grid-column: 2 / span 1;
                    grid-row: 1 / span 2;
                    position: relative;
                }

                page-desafio-segunda div[product] img{
                    position: absolute;
                    top:0;
                    right:0;
                    max-width:100%;
                    width:100%;
                    max-height: 100%;
                    object-fit: contain;
                    object-position: right center;
                }

                figure{
                    padding: 0;
                    margin:0px
                }

                figcaption{
                    font-size: .8em;
                }

                page-desafio-segunda #dados>img, page-desafio-segunda #dados>figure>img{
                    max-width:100%;
                }

                /*
                page-desafio-segunda div[product] > div{
                    position:relavive;
                    background-size: cover;
                    background-position:center center;
                }*/


                page-desafio-segunda p, ol{
                    font-weight: 100;
                    font-size: var(--textSizeBody);
                    margin:0;
                    line-height:1.5em;
                }

                page-desafio-segunda ol{
                    list-style-position: inside;
                    padding-left:0;
                    margin:0;
                }

                page-desafio-segunda li{
                    margin-bottom: 2em;
                }

                page-desafio-segunda ol li ul li{
                    margin-bottom: 0;
                    font-size: var(--textSizeSource);
                    font-weight: 100;
                }

                page-desafio-segunda ol li ul li.blue{
                    color: var(--colorLightBlue);
                }

                page-desafio-segunda #participe{
                    background-color:var(--colorDarkBlue);
                    color:white:
                }

                page-desafio-segunda #participe ol{
                    column-count: 1;
                    column-gap: var(--verticalPadding);
                    color:white;
                    font-size: initial;
                    font-weight: 500;
                }

                @media (min-width: 768px) {
                    page-desafio-segunda #participe ol{
                        column-count: 2;
                    }
                }



                page-desafio-segunda #participe ol p{
                    display:block;
                    font-size: var(--textSizeSource);
                    font-weight: 100;
                    margin: .5em 0 ;
                }
                page-desafio-segunda #participe [break]{
                    break-after: column;
                    margin-bottom: 0;
                }

                page-desafio-segunda #participe ol p:first-child{
                    margin-top:1em;
                }

                page-desafio-segunda a:link, page-desafio-segunda a:visited , page-desafio-segunda a:hover , page-desafio-segunda a:active{
                    color: inherit;
                }

                page-desafio-segunda .people{
                    display: grid;
                    grid-gap: var(--sidePadding);
                    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
                }


                /* YOUTUBE EMBED*/

                .embed-container {
                    position: relative;
                    padding-bottom: 56.25%;
                    height: 0;
                    overflow: hidden;
                    max-width: 100%;
                }
                
                .embed-container iframe, .embed-container object, .embed-container embed {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                }
            </style>
            <div>
                <section id="top">
                    <div>
                        <a href="https://marcaneve.com.br/">
                            <img src="./assets/logo-neve-shadow.svg" alt="">
                        </a>
                        <span>Apresenta</span>
                    </div>
                    <h1>Banheiros Mudam Vidas</h1>
                    <h2>O Desafio</h2>

                    <a href="/regulamento">
                        <div id="saiba-mais">Saiba Mais</div>
                    </a>
                </section>

                <section id="dados">
                    <h1>Sua iniciativa pode mudar o saneamento básico.</h1>
                    <p>
                        Quase metade da população brasileira não possui saneamento completo, e esse problema impacta de forma direta na educação, na saúde e na vida das pessoas. Por isso, Neve está sempre participando de muitos programas que buscam levar soluções relevantes para as mais diversas comunidades brasileiras. Como o <i>Banheiros Mudam Vidas – o Desafio</i>, um programa que apoia iniciativas que objetivam transformar esse cenário. E você também pode fazer parte disso.
                    </p>

                    <br>

                    <h2>O saneamento básico precisa de cuidados:</h2>
                    <info-group >
                        <span slot="major">+ de <span>4</span> milhões</span>
                        <span slot="text">de brasileiros não têm acesso a banheiro.</span>
                    </info-group>

                    <info-group>
                        <span slot="major">+ de <span>96</span> milhões</span>
                        <span slot="text">não têm acesso a coleta e tratamento de esgoto.</span>
                    </info-group>

                    <br>

                    <!-- 
                    <div>
                        <h1>Acompanhe tudo o que acontece durante o desafio.</h1>
                    </div>
                    <div class='embed-container'><iframe  src='https://www.youtube.com/embed/QILiHiTD3uc'  allow="display" frameborder='0' allowfullscreen></iframe></div>

                    <br>
                    -->

                    <h1>Quer saber como contribuir com essas iniciativas?</h1>
                    <div product >
                        <p>
                        Na compra do produto exclusivo Neve Toque Suave, você ajuda a alavancar essas ações focadas na melhoria do saneamento básico no Brasil.
                        </p>
                        <simple-button href="https://www.carrefour.com.br/Papel-Higienico-Folha-Dupla-30-Metros-Neve-Toque-da-Seda-com-12-Unidades/p/5811031?validateUnavailableItems=true" target="_blank">Compre Aqui</simple-button>
                        <div>
                            <img src="./assets/produto.jpg" alt="">
                        </div>
                    </div>

                    <br>

                    <h1>Camila Farani também apoia: <i>Banheiros Mudam Vidas – o Desafio.</i></h1>
                    <div sidePhoto>
                        <div style='background-image:url(./assets/camila.jpg)'></div>
                        <p>
                            Camila Farani é um dos “tubarões” do Shark Tank Brasil, que já está na sua quarta temporada. Ela é considerada a maior investidora-anjo do Brasil, sendo a única mulher bicampeã do Startup Awards, premiada como Melhor Investidora-Anjo em 2016 e 2018. Camila é sócia-fundadora da G2 Capital, uma butique de investimentos em startups de tecnologia e, de 2016 a 2018, presidiu o Gávea Angels – um dos primeiros grupos de investimento-anjo do Brasil. Sempre muito ativa nas causas femininas, em 2014 cofundou o grupo Mulheres Investidoras Anjo, de incentivo a mulheres empreendedoras. Camila empreende desde 2001, como sócia e criadora do Grupo Boxx (alimentação) e, mais recentemente, com sua empresa de consultoria e educação, a Innovaty. É advogada e tem pós-graduação em Marketing e especializações em Empreendedorismo e Inovação por Stanford e pelo Massachusetts Institute of Technology (MIT).
                        </p>
                    </div>

                    <br>

                    <h1>Conheça quem faz o programa acontecer.</h1>
                    <figure>
                        <img src="./assets/KC_compressed.jpg" alt="">
                        <figcaption>Créditos da foto: André Hanni</figcaption>
                        <simple-button href="/startups">Conheça as startups selecionadas</simple-button>
                    </figure>
                    

                    <h1>Conheça alguns dos nossos mais de 80 voluntários!</h1>
                    <p>Juntos somos mais fortes, e é por isso que temos um timaço de voluntários da K-C prontos para ajudar em todos os temas que as iniciativas poderão solicitar.</p>
                    <div class="people">
                        <photo-card src='./assets/people/Ana.V.Costa@kcc.com.jpg'>Ana Costa</photo-card>
                        <photo-card src='./assets/people/analuiza.carneiro@kcc.com.jpg'>Ana Luiza Carneiro</photo-card>
                        <photo-card src='./assets/people/Camila.Martins@kcc.com.jpg'>Camila Martins</photo-card>
                        <photo-card src='./assets/people/carolina.Almeida@kcc.com.jpg'>Carolina Almeida</photo-card>
                        <photo-card src='./assets/people/chiara.mandelli@kcc.com.jpg'>Chiara Mandelli</photo-card>
                        <photo-card src='./assets/people/Eduardo.E.Martinez@kcc.com.jpg'>Eduardo Martinez</photo-card>
                        <photo-card src='./assets/people/Erico.Melo@kcc.com.jpg'>Erico Melo</photo-card>
                        <photo-card src='./assets/people/Eugenia.Santos@kcc.com.jpg'>Eugênia Santos</photo-card>
                        <photo-card src='./assets/people/Felipe.Costa3@kcc.com.jpg'>Felipe Costa</photo-card>
                        <photo-card src='./assets/people/gabriel.Grandi@kcc.com.jpg'>Gabriel Grandi</photo-card>
                        <photo-card src='./assets/people/Gabriela.Moura1@kcc.com.jpg'>Gabriela Moura</photo-card>
                        <photo-card src='./assets/people/Giuliana.M.Guazzelli@kcc.com.jpg'>Giuliana Guazzelli</photo-card>
                        <photo-card src='./assets/people/helen.pereira1@kcc.com.jpg'>Helen Pereira</photo-card>
                        <photo-card src='./assets/people/Jessica_Steinhoff.jpg'>Jessica Steinhoff</photo-card>
                        <photo-card src='./assets/people/karine.Andrade@kcc.com.jpg'>Karine Andrade</photo-card>
                        <photo-card src='./assets/people/Larissa.Pissimilio@kcc.com.jpg'>Larissa Pissimilio</photo-card>
                        <photo-card src='./assets/people/Larissa.Spengler@kcc.com.jpg'>Larissa Spengler</photo-card>
                        <photo-card src='./assets/people/Leticia.Souza@kcc.com.jpg'>Leticia Souza</photo-card>
                        <photo-card src='./assets/people/Ligia.B.Silva@kcc.com.jpg'>Ligia Silva</photo-card>
                        <photo-card src='./assets/people/Ludmilla.Pereira@kcc.com.jpg'>Ludmilla Pereira</photo-card>
                        <photo-card src='./assets/people/mayara_goncalves.jpg'>Mayara Gonçalves</photo-card>
                        <photo-card src='./assets/people/Mayara.Rigolo@kcc.com.jpg'>Mayara Rigolo</photo-card>
                        <photo-card src='./assets/people/Michael.A.Vaz@kcc.com.jpg'>Michael Vaz</photo-card>
                        <photo-card src='./assets/people/Michele.Nascimento@kcc.com.jpg'>Michele Nascimento</photo-card>
                        <photo-card src='./assets/people/Nilo.Rosa@kcc.com.jpg'>Nilo Rosa</photo-card>
                        <photo-card src='./assets/people/pedro_ornellas.jpg'>Pedro Ornellas</photo-card>
                        <photo-card src='./assets/people/raphaella.barbosa@kcc.com.jpg'>Raphaella Barbosa</photo-card>
                        <photo-card src='./assets/people/Rodolfo.O.Diniz@kcc.com.jpg'>Rodolfo Diniz</photo-card>
                        <photo-card src='./assets/people/Soraia.Marioti@kcc.com.jpg'>Soraia Marioti</photo-card>
                        <photo-card src='./assets/people/Thais_Pacheco_Tanganelli.jpg'>Thaís Tanganelli</photo-card>
                        <photo-card src='./assets/people/Thaissa.Gentil@kcc.com.jpg'>Thaissa Gentil</photo-card>
                        <photo-card src='./assets/people/victor.madoenho@kcc.com.jpg'>Victor Madoenho</photo-card>
                        <photo-card src='./assets/people/vitor.marco@kcc.com.jpg'>Vitor Marco</photo-card>
                        <photo-card src='./assets/people/Walter.O.Batista@kcc.com.jpg'>Walter Batista</photo-card>
                    </div>
                </section>
                

                <section id="participe">
                    <h1>Etapas do Programa</h1>

                    <ol>
                        <li>
                            Inscrição
                            <p>De 17 de julho de 2019 até o fim do dia 25 de agosto de 2019.</p>
                        </li>

                        <li>
                            Selecionados para a oficina de pré-seleção
                            <p>A divulgação dos selecionados para a oficina será feita entre 16 e 30 de setembro de 2019.</p>
                        </li>
                        
                        <li>
                            Oficina de pré-seleção
                            <p>De 8 a 10 de outubro de 2019; contaremos com 12 iniciativas.</p>
                        </li>
                        <li>
                            Programa de Aceleração
                            <p>A divulgação dos 8 a 10 selecionados para o Programa de Aceleração será feita até 11 de outubro. As oficinas serão realizadas entre 14/10/2019 e 19/02/2020.</p>
                            <ul>
                                <li>Oficina intermediária: de 10 a 12 de dezembro de 2019.</li>
                                <li>Oficina de encerramento: 18 e 19 de fevereiro de 2020.</li>
                                <li class="blue">Teremos webinars e  calls ao longo dos 4 meses do programa.</li>
                            </ul>
                        </li>

                        <li>
                            Acompanhamento para as 4 iniciativas premiadas com a aceleração 
                            <p>de março a agosto de 2020.</p>
                        </li>
                    </ol>
                    <simple-button href="/startups">Conheça as startups selecionadas</simple-button>
                </section>
            </div>
        `;
    }
    connectedCallback() {
        console.log('Custom square element added to page.');
    }

    disconnectedCallback() {
        console.log('Custom square element removed from page.');
    }

    adoptedCallback() {
        console.log('Custom square element moved to new page.');
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Custom square element attributes changed.');
    }
}
customElements.define('page-desafio-segunda', PageDesafioSegunda);