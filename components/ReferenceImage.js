export class ReferenceImage extends HTMLElement{
    static get observedAttributes() {
        return ['src', 'label'];
    }
    

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    background-size: cover;
                    background-image: var(--srcImg);
                    background-position: right top;
                    padding-top:100%;
                }/*COPIA PARA EDGE*/
                reference-image
                {
                    background-size: cover;
                    background-image: var(--srcImg);
                    background-position: right top;
                    padding-top:100%;
                }
            </style>
        `;
        this._shadow = shadowRoot;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if(name=="src" && newValue && oldValue!=newValue){
            this._shadow.host.style.setProperty("--srcImg",`url(${newValue})`)
        }
    }

}
customElements.define('reference-image', ReferenceImage);

