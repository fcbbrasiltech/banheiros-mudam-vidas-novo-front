import {PlayButton} from "./PlayButton.js"

export class VideoStory extends HTMLElement{
    static get observedAttributes() {
        return ['imgsrc','youtube'];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    position:relative;
                    background-image:var(--bgImage);
                    background-size:cover;
                    background-position: left center;
                    display:block;
                }
                /*COPIA PARA EDGE*/
                video-story{
                    position:relative;
                    background-image:var(--bgImage);
                    background-size:cover;
                    background-position: left center;
                    display:block;
                }

                #grid{
                    display: grid;
                    
                    height:100%;
                    width:100%;
                    position:absolute;
                    top:0px;
                    left:0px;

                    box-sizing: border-box;
                    padding: calc(var(--sidePadding) / 2);

                    grid-template-columns: 100%;
                    grid-template-rows: calc(100% - 60px - (var(--sidePadding) / 2)) 60px;
                    overflow:hidden;
                }

                :host([left]){
                    background-position: right center;
                }
                /*COPIA PARA EDGE*/
                video-story[left]{
                    background-position: right center;
                }

                .top{
                    font-size:var(--textSizeHeading);
                    line-height: calc(var(--textSizeHeading) * 1.2);
                    visibility: hidden;
                }

                :host([left]) .top{
                    visibility: visible;
                }
                /*COPIA PARA EDGE*/
                video-story[left] .top{
                    visibility: visible;
                }

                :host([right]) .top{
                    text-align: right;
                    visibility: visible;
                }
                /*COPIA PARA EDGE*/
                video-story[right] .top{
                    text-align: right;
                    visibility: visible;
                }

                .bottom{
                    color:white;
                    text-align: right;
                    font-weight: 500;
                    display: grid;
                    grid-template-columns: auto calc(var(--sidePadding) / 2) 60px;
                }

                play-button{
                    grid-column: 3 / span 1;
                    grid-row: 1 / span 3;
                    height:100%;
                }

                ::slotted([slot]){
                    grid-column: 1 / span 1;
                }
                /*COPIA PARA EDGE*/
                video-story .bottom>*[slot]{
                    grid-column: 1 / span 1;
                }

                ::slotted([slot=quote]){                   
                    white-space:initial;
                    text-align: justify;
                    margin-bottom: .5em;
                    font-size: var(--textSizeBody);
                }
                /*COPIA PARA EDGE*/
                video-story .bottom>*[slot=quote]{                   
                    white-space:initial;
                    text-align: justify;
                    margin-bottom: .5em;
                    font-size: var(--textSizeBody);
                }

                ::slotted([slot=name]){
                    font-size: var(--textSizeBody);
                }
                /*COPIA PARA EDGE*/
                video-story .bottom>*[slot=name]{
                    font-size: var(--textSizeBody);
                }

                ::slotted([slot=prof]){
                    font-weight: 100;
                    font-size:var(--textSizeSource);
                }
                /*COPIA PARA EDGE*/
                video-story .bottom>*[slot=prof]{
                    font-weight: 100;
                    font-size:var(--textSizeSource);
                }

                .unselect{
                    -webkit-touch-callout: none; /* iOS Safari */
                    -webkit-user-select: none; /* Safari */
                    -khtml-user-select: none; /* Konqueror HTML */
                    -moz-user-select: none; /* Firefox */
                        -ms-user-select: none; /* Internet Explorer/Edge */
                            user-select: none;
                    
                }
            </style>
            <div id="grid">
                <div class="top unselect" >
                    # MEU
                    <br>
                    PRIMEIRO
                    <br>
                    BANHEIRO
                </div>
                <div class="bottom">
                    <slot class="unselect" name="quote"></slot>
                    <slot class="unselect" name="name"></slot>
                    <slot class="unselect" name="prof"></slot>
                    <play-button></play-button>
                </div>
            </div>
        `;
        this._shadow = shadowRoot;
        this.playButton=this._shadow.querySelector("play-button");
        this.playButton.addEventListener("click", ()=>{
            window.playVideo(this.youtube)
        })
        this.youtube=undefined;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        
        if (newValue && oldValue!=newValue) {
            if(name=='imgsrc'){
                this._shadow.host.style.setProperty("--bgImage", 'url('+newValue.toString()+')')
            }
            if(name=='youtube'){
                this.youtube=newValue;
            }
        }
    }
}
customElements.define('video-story', VideoStory);