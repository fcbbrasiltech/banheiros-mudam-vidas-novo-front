export class CustomDropdown extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    position:relative;
                    width:100%;
                    display:block;
                }
                /*COPIA PARA EDGE*/
                custom-dropdown{
                    position:relative;
                    width:100%;
                    display:block;
                }
                

                ::slotted([slot=field]){
                    cursor:pointer;
                }
                /*COPIA PARA EDGE*/
                custom-dropdown>*[slot=field]{
                    cursor:pointer;
                }

                :host(:not([debug])) ::slotted([slot=list]:not([active])){
                    display:none;
                }
                /*COPIA PARA EDGE*/
                custom-dropdown>*:not([debug]) custom-dropdown>*[slot=list]:not([active]){
                    display:none;
                }

                ::slotted([slot=list][active]){
                    z-index:1;
                    display:initial;
                }
                /*COPIA PARA EDGE*/
                custom-dropdown>*[slot=list][active]{
                    z-index:1;
                    display:initial;
                }
                
            </style>
            <slot name="field"></slot>
            <slot name="list"></slot>
        `;
        this._shadow = shadowRoot;
        this.fieldSlot=this._shadow.querySelector("slot[name=field]");
        this.listSlot=this._shadow.querySelector("slot[name=list]");

        let setNodeChildObserver=()=>{
            
            let lists
            if(this.listSlot.assignedElements){
                lists=this.listSlot.assignedElements();      
            }else{
                lists=this.listSlot.assignedNodes();
                lists=els.filter(a=>{
                    let name=a.constructor.name;
                    let r=false;

                    if(name!="Text"){
                        r=true;
                    }
                    return r
                })
            }
            
            this.list=lists[0];
            this.listChange();
            const config = { attributes: false, childList: true, subtree: false };
            const observer = new MutationObserver(this.listChange.bind(this));
            observer.observe(this.list, config);
            this.listSlot.removeEventListener("slotchange", setNodeChildObserver);
            
        }

        this.fieldSlot.addEventListener("slotchange", this.fieldChange.bind(this));
        this.listSlot.addEventListener("slotchange", setNodeChildObserver);

        this.query="";
        this.selected=undefined;
        this.hover=undefined;
        this.timeout=undefined;
    }

    

    listChange(){
        let lists
        if(this.listSlot.assignedElements){
            lists=this.listSlot.assignedElements();      
        }else{
            lists=this.listSlot.assignedNodes();
            lists=els.filter(a=>{
                let name=a.constructor.name;
                let r=false;

                if(name!="Text"){
                    r=true;
                }
                return r
            })
        }
        
        if(lists.length!=1){
            console.error("O numero de listas deve ser 1")
        }else{
            this.listItems=Array.from(this.list.children);
            
            this.listItems=this.listItems.map((el, index)=>{
                
                el.addEventListener("click", this.itemClick.bind(this));
                el.addEventListener("mousemove", this.itemHover.bind(this));

                let obj={};
                for (let att, i = 0, atts = el.attributes, n = atts.length; i < n; i++){
                    att = atts[i];
                    obj[att.nodeName]=att.nodeValue;
                }

                let data={
                    index:index,
                    element:el,
                    content:el.innerHTML,
                    attributes:obj
                }

                el.data=data;

                return data
            })
        }
        
        
        this.list.removeAttribute("active")

        this.query="";
        this.selected=undefined;
    }

    clear(){
        this.selected=undefined;
        this.field.value="";
        this.query="";
        this.blur();
        this.listItems.forEach(item => {
            if(item.element.parentElement){
                item.element.parentElement.removeChild(item.element);
            }
        });
    }
    

    itemHover(e){
        if(this.hover){
            this.hover.element.removeAttribute("hover");
        }
        this.hover=e.currentTarget.data;
        this.hover.element.setAttribute("hover", "");
    }

    itemClick(e){
        
        this.selected=e.currentTarget.data;
        this.changeEvent();
        this.query=this.selected.content;
        this.field.value=this.selected.content
        
        clearTimeout(this.timeout);
        this.blur();
    }

    changeEvent(){
        let event = new CustomEvent("onchange", {
                bubbles: true,
                cancelable: false,
                detail: this.selected
            }
        );

        this.dispatchEvent(event);
    }
    
    fieldChange(){
        let fields
        if(this.fieldSlot.assignedElements){
            fields=this.fieldSlot.assignedElements();      
        }else{
            fields=this.fieldSlot.assignedNodes();
            fields=els.filter(a=>{
                let name=a.constructor.name;
                let r=false;

                if(name!="Text"){
                    r=true;
                }
                return r
            })
        }

        if(fields.length!=1){
            console.error("O numero de campos deve ser 1")
        }else{
            this.field=fields[0].querySelector("input");
            this.field.addEventListener("input", this.fielInput.bind(this));
            this.field.addEventListener("blur", this.blur.bind(this));
            this.field.addEventListener("focus", this.focus.bind(this));
            
            this.dropButton=fields[0].querySelector("[dropbutton]");
            if(this.dropButton){
                this.dropButton.addEventListener("click", this.dropClick.bind(this));
            }
        }
    }

    dropClick(e){
        this.list.setAttribute("active", "")
        if(this.list.scrollTo){
            this.list.scrollTo(0,0);
        }else{
            this.list.scrollTop=0;
        }
        
        this.field.focus();
        
    }

    focus(){
        this.updateList();
    }

    blur(e){
        this.timeout=setTimeout(() => {
            if(!this.selected){
                this.field.value="";
            }
            this.list.removeAttribute("active")

            if(this.hover){
                this.hover.element.removeAttribute("hover");
            }
            this.hover=undefined;
            if(this.list.scrollTo){
                this.list.scrollTo(0,0);
            }else{
                this.list.scrollTop=0;
            }
        }, 100);
    }

    fielInput(){
        this.query=this.field.value;
        this.updateList();
    }

    updateList(){
        let newQuery=this.removeAcentos(this.query).toLowerCase();

        let contem=false;
        this.listItems.forEach(item => {
            let check=this.removeAcentos(item.content).toLowerCase();
            if(check.indexOf(newQuery)>-1){
                contem=true;
                item.element.style.display="";
            }else{
                item.element.style.display="none";
            }
        });

        if(contem){
            this.list.setAttribute("active", "")
        }else{
            this.list.removeAttribute("active")
        }
        if(this.list.scrollTo){
            this.list.scrollTo(0,0);
        }else{
            this.list.scrollTop=0;
        }
    }

    removeAcentos(text){
        const a = 'àáäâãèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
        const b = 'aaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
        const p = new RegExp(a.split('').join('|'), 'g')
        return text.toString().toLowerCase().trim()
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special chars
            .replace(/&/g, '-and-') // Replace & with 'and'
            .replace(/[\s\W-]+/g, '-') // Replace spaces, non-word characters and dashes with a single dash (-)
    }
}
customElements.define('custom-dropdown', CustomDropdown);