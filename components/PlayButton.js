export class PlayButton extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    background-color:rgba(255,255,255,.5);
                    background-image:url(./assets/play-solid.svg);
                    background-size: 50% auto;
                    background-repeat: no-repeat;
                    background-position: center center;
                    cursor:pointer;
                }/*COPIA PARA EDGE*/
                play-button{
                    background-color:rgba(255,255,255,.5);
                    background-image:url(./assets/play-solid.svg);
                    background-size: 50% auto;
                    background-repeat: no-repeat;
                    background-position: center center;
                    cursor:pointer;
                }

                img{
                    height:100%;
                    pointer-events:none;
                }
            </style>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAD0lEQVR42mNkwAIYh7IgAAVVAAuInjI5AAAAAElFTkSuQmCC">
        `;
        this._shadow = shadowRoot;
    }

}
customElements.define('play-button', PlayButton);