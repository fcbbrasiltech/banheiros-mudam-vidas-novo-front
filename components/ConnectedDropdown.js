import {CustomDropdown} from "./CustomDropdown.js"

export class ConnectedDropdown extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style></style>
            <slot></slot>
        `;
        this._shadow = shadowRoot;

        this._slot = this._shadow.querySelector('slot');
        this.scriptElement=undefined;
        this.dataContent=undefined;
        this.drops=[];
        this._slot.addEventListener('slotchange', this.slotChange.bind(this));
        
    }

    slotChange(e){
        if(this._slot.assignedElements){
            this.nodes=this._slot.assignedElements();      
        }else{
            let els=this._slot.assignedNodes();
            this.nodes=els.filter(a=>{
                let name=a.constructor.name;
                let r=false;

                if(name!="Text"){
                    r=true;
                }
                return r
            })
        }

        let scriptCont=0;
        this.nodes.forEach(element => {
            if (element instanceof HTMLScriptElement && element.getAttribute("type")=="application/json"){
                scriptCont++;
                this.scriptElement=element;
            }else if(element instanceof CustomDropdown){
                element.levelId=this.drops.length;
                element.addEventListener("onchange", this.changeDrop.bind(this))
                this.drops.push(element)
            }else{
                console.error(`Tipo de elemento '${element.constructor.name}' não é aceito. Use 'CustomDropdown'.`)
            }
        });

        if(scriptCont!==1){
            console.error(`ConnectedDropdown deve conter UM script do tipo 'application/json'.`)
        }else{
            
            try{
                this.dataContent=JSON.parse(this.scriptElement.innerHTML)
            }catch(e){
                let src=this.scriptElement.getAttribute("src");
                if(src.length>0){
                    this.xhr(src, (json)=>{
                        if(this.isArray(json)){
                            this.dataContent=json;
                            this.processJson()
                            this.clearAll();
                            this.populate(this.dataContent, 0);
                        }else{
                            console.error(`JSON precisa ser JSONArray`)
                        }
                        
                    })
                }else{
                    console.error(`Não foi encontrado nenhum JSON válido`)
                }
            }
        }
    }

    clearAll(){
        for(let i = 0; i<this.drops.length; i++){
            let item=this.drops[i].querySelector("[slot='list']").children[0];
            this.drops[i].itemClone=item;
            this.drops[i].clear()
        }
    }

    changeDrop(e){
        let level=e.target.levelId;
        
        if(level<this.level-1){
            let drop=e.target;
            let data=e.detail;

            let newScope=this.dataContent.filter((el)=>{return el.label===data.content})[0].child
            this.populate(newScope, level+1);
        }

        for(let i = level+1; i<this.drops.length; i++){
            this.drops[i].clear()
        }
    }

    isArray(what) {
        return Object.prototype.toString.call(what) === '[object Array]';
    }

    xhr(url, callback, type){
        var req = new XMLHttpRequest();
        req.open( type || 'GET', url, true);

        req.responseType = 'json';

        req.onload = () => {
            if (req.readyState === req.DONE) {
                if (req.status === 200) {
            
                    callback(req.response);
                }
            }
        };

        req.error=()=>{
            console.error(`Não foi encontrado nenhum JSON válido`)
        }

        req.send(null);
    }

    processJson(){
        let clone=Array.from(this.dataContent)
        let process=this.processLevel(clone);
        this.dataContent=process.newArray;
        this.level=process.level;

        if(this.level!=this.drops.length){
            console.error(`Seus dados tem ${this.level} ${this.level > 1 ? 'níveis' : 'nível'} porém ${this.drops.length} ${this.drops.length > 1 ? 'instancias' : 'instancia'} de 'CustomDropdown'`);
        }
    }

    populate(scope, level){
        let drop=this.drops[level];
        drop.scope=scope;

        let list=drop.querySelector("[slot='list']");
        
        if(list && drop.itemClone){
            let item = drop.itemClone;
            scope.forEach(dataItem => {
                let clone=item.cloneNode();
                clone.innerHTML=dataItem.label;
                list.appendChild(clone);
            });
        }
        
    }

    processLevel(array){
        let levelUp=0;

        let retorno = array.map((item)=>{
            let r = item;
            if(item instanceof Object){
                if(item.child && item.child instanceof Array && item.child.length>0){
                    // console.log(item)
                    let process=this.processLevel(item.child);
                    item.child=process.newArray
                    if(levelUp<process.level){
                        levelUp=process.level
                    }
                }
            }else{
                r={
                    "label":String(item),
                }
            }
            
            return r;
        })
        
        return {newArray: retorno, level:1+levelUp};
    }

    // connectedCallback() {
    //     console.log('Custom square element added to page.');
    // }

    // disconnectedCallback() {
    //     console.log('Custom square element removed from page.');
    // }

    // adoptedCallback() {
    //     console.log('Custom square element moved to new page.');
    // }

    // attributeChangedCallback(name, oldValue, newValue) {
    //     console.log('Custom square element attributes changed.');
    // }
}
customElements.define('connected-dropdown', ConnectedDropdown);