export class SquareGallery extends HTMLElement{
    static get observedAttributes() {
        return ["disable"];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    
                }

                :host([dots]) #dots{
                    display:grid;
                }
                /*COPIA PARA EDGE*/
                square-gallery[dots] #dots{
                    display:grid;

                    
                }

                #dots{
                    position:absolute;
                    top:-1em;
                    left:calc(var(--sidePadding) + var(--show));
                    width: calc(100% - (2 * var(--sidePadding)) - (2 * var(--show)));
                    display: none;
                    grid-template-columns: repeat(var(--nSlots), 1fr);
                    grid-gap: .5em;
                }

                #dots>div{
                    height:.1em;
                    background-color:var(--colorBlack);
                    transition: background-color .5s;
                }

                #dots>div[selected]{
                    background-color:white;
                }

                :host([disable]) #dots{
                    display:none;
                }
                /*COPIA PARA EDGE*/
                square-gallery[disable] #dots{
                    display:none;
                }

                :host(:not([disable])) {
                    display:block;
                    position:relative;
                    --spacing: 14pt;
                    --show: 1em;

                    width:100%;
                    padding-top:calc(100% -  (2 * (var(--spacing) + var(--show))) );
                }
                /*COPIA PARA EDGE*/
                square-gallery:not([disable]) {
                    display:block;
                    position:relative;
                    --spacing: 14pt;
                    --show: 1em;

                    width:100%;
                    padding-top:calc(100% -  (2 * (var(--spacing) + var(--show))) );
                }

                :host([disable]){
                    padding-top:initial;
                }
                /*COPIA PARA EDGE*/
                square-gallery[disable]{
                    padding-top:initial;
                }


                .scroller{
                    position: absolute;
                    overflow-y: hidden;
                    overflow-x: auto;
                    scroll-snap-type: x mandatory;
                    top:0;
                    left:0;
                    width: 100%;
                    height: 100%;
                    white-space:nowrap;
                    font-size:0px;
                    -webkit-overflow-scrolling: touch;
                }

                :host([disable]) .scroller{
                    position:relative;
                    
                    display:var(--my-display);
                    grid-template-columns:var(--my-grid-template-columns);
                    grid-gap:var(--my-grid-gap);
                }
                /*COPIA PARA EDGE*/
                square-gallery[disable] .scroller{
                    position:relative;
                    
                    display:var(--my-display);
                    grid-template-columns:var(--my-grid-template-columns);
                    grid-gap:var(--my-grid-gap);
                }


                :host(:not([disable])) ::slotted(*:first-child){
                    margin-left: calc(var(--spacing) + var(--show));
                }
                /*COPIA PARA EDGE*/
                square-gallery:not([disable])) .scroller>*:first-child{
                    margin-left: calc(var(--spacing) + var(--show));
                }

                :host(:not([disable])) ::slotted(*:last-child){
                    margin-right: calc(var(--spacing) + var(--show));
                }
                /*COPIA PARA EDGE*/
                square-gallery:not([disable]) .scroller>*:last-child{
                    margin-right: calc(var(--spacing) + var(--show));
                }

                :host(:not([disable])) ::slotted(*){
                    margin-right: var(--spacing);
                    position:relative;
                    width:calc(100% - (2 * (var(--spacing) + var(--show))));
                    height:calc(100% );
                    display: inline-block;
                    scroll-snap-align:center;
                    font-size: 12pt;
                    box-sizing:border-box;
                    object-fit: cover;
                }
                /*COPIA PARA EDGE*/
                square-gallery:not([disable]) .scroller>*{
                    margin-right: var(--spacing);
                    position:relative;
                    width:calc(100% - (2 * (var(--spacing) + var(--show))));
                    height:calc(100% );
                    display: inline-block;
                    scroll-snap-align:center;
                    font-size: 12pt;
                    box-sizing:border-box;
                    object-fit: cover;
                }

                :host([disable]) ::slotted(*){
                    width: auto;
                    padding-top:100%;
                }
                /*COPIA PARA EDGE*/
                square-gallery[disable] .scroller>*{
                    width: auto;
                    padding-top:100%;
                }
            </style>

            <div id="dots"></div>
            <div class="scroller">
                <slot></slot>
            </div>
        `;
        this._shadow = shadowRoot;
        this.scroller = this._shadow.querySelector(".scroller");
        this.dotsContainer = this._shadow.querySelector("#dots");
        this.nodes=[];
        this.selectedNodeId=0;
        this._slot = this._shadow.querySelector('slot');

        this._bindScroll=this.scrollDetection.bind(this);
        
        this._slot.addEventListener('slotchange', this.slotChange.bind(this));
        this.checkEnabled();
    }
    
    slotChange(e){
        if(this._slot.assignedElements){
            this.nodes=this._slot.assignedElements();      
        }else{
            let els=this._slot.assignedNodes();
            this.nodes=els.filter(a=>{
                let name=a.constructor.name;
                let r=false;

                if(name!="Text"){
                    r=true;
                }
                return r
            })
        }      
        this._shadow.host.style.setProperty("--nSlots", this.nodes.length);
        this.dotsContainer.innerHTML="";

        this.dots=[];
        this.nodes.forEach(element => {
            let d=document.createElement("div");
            this.dots.push(d);
            this.dotsContainer.appendChild(d);
        });

        this.currentNode.setAttribute("selected", "");
        this.currentDot.setAttribute("selected", "")
    }

    scrollDetection(e){
        e.preventDefault()
        e.stopPropagation()

        let left=this.scroller.scrollLeft;

        let menor=10000;
        let id=-1;
        this.nodes.forEach((element, i) => {
            if(Math.abs(left-element.offsetLeft)<menor){
                menor=Math.abs(left-element.offsetLeft)
                id=i;
            }
        });

        
        
        if(this.selectedNodeId!=id){
            this.changeSelected(id);
        }

        return false
    }

    changeSelected(id){
        this.currentNode.removeAttribute("selected");
        this.currentDot.removeAttribute("selected");

        this.selectedNodeId=id;
        let node=this.currentNode;
        
        this.currentNode.setAttribute("selected", "");
        this.currentDot.setAttribute("selected", "");

        let obj={};
        for (let att, i = 0, atts = node.attributes, n = atts.length; i < n; i++){
            att = atts[i];
            obj[att.nodeName]=att.nodeValue;
        }

        let event = new CustomEvent("selectionChanged", {
                bubbles: true,
                cancelable: false,
                detail: {
                    id: id,
                    current: this.currentNode,
                    attributes:obj
                }
            }
        );


        this.dispatchEvent(event);
    }

    get currentNode(){
        return this.nodes[this.selectedNodeId] || undefined;
    }

    get currentDot(){
        return this.dots[this.selectedNodeId] || undefined;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if(name=="disable"){
            this.checkEnabled();
        }
    }

    checkEnabled(){
        if(this.hasAttribute("disable")){
            this.disable=true;
            this.scroller.removeEventListener('scroll', this._bindScroll,false);
        }else{
            this.disable=false;
            this.scroller.addEventListener('scroll', this._bindScroll,false);
        }
    }

}
customElements.define('square-gallery', SquareGallery);