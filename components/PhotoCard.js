export class PhotoCard extends HTMLElement{
    static get observedAttributes() {
        return [];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    display: grid;
                    text-align:center;
                    grid-template-columns: 1fr;
                }
                /*COPIA PARA EDGE*/
                photo-card{
                    display: grid;
                    text-align:center;
                    grid-template-columns: 1fr;
                }
                

                :host img{
                    width:100%;
                    float:left;
                    visibility:hidden;
                }
                /*COPIA PARA EDGE*/
                photo-card img{
                    width:100%;
                    float:left;
                    visibility:hidden;
                }

                :host .container{
                    background-size: cover;
                    background-position: center center;
                    background-repeat: no-repeat;
                }
                /*COPIA PARA EDGE*/
                photo-card .container{
                    background-size: cover;
                    background-position: center center;
                    background-repeat: no-repeat;
                }
            </style>

<!-- style="background-image:url(${this.getAttribute('src')});" -->

            <div class="container" style="background-image:url(${this.getAttribute('src')});"><img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHdpZHRoPSI0NTMuNTRweCIgaGVpZ2h0PSIyNTUuMTJweCIgdmlld0JveD0iMCAwIDQ1My41NCAyNTUuMTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ1My41NCAyNTUuMTI7IgoJIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPgoJLnN0MHtmaWxsOiMxQjI1NTA7fQoJLnN0MXtmaWxsOiMxOUJBRDA7fQo8L3N0eWxlPgo8ZGVmcz4KPC9kZWZzPgo8cmVjdCBjbGFzcz0ic3QwIiB3aWR0aD0iNDUzLjU0IiBoZWlnaHQ9IjI1NS4xMiIvPgo8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjg5LDE1My4zM2MtOS41OC05LjU4LTIwLjk5LTE2LjY4LTMzLjQ0LTIwLjk4YzEzLjMzLTkuMTgsMjIuMDktMjQuNTUsMjIuMDktNDEuOTIKCWMwLTI4LjA1LTIyLjgyLTUwLjg4LTUwLjg3LTUwLjg4Yy0yOC4wNSwwLTUwLjg4LDIyLjgyLTUwLjg4LDUwLjg4YzAsMTcuMzgsOC43NiwzMi43NCwyMi4wOSw0MS45MgoJYy0xMi40NSw0LjI5LTIzLjg2LDExLjM5LTMzLjQ0LDIwLjk4Yy0xNi42MiwxNi42Mi0yNS43NywzOC43Mi0yNS43Nyw2Mi4yM2gxMy43NWMwLTQwLjk0LDMzLjMxLTc0LjI1LDc0LjI1LTc0LjI1CgljNDAuOTQsMCw3NC4yNSwzMy4zMSw3NC4yNSw3NC4yNWgxMy43NUMzMTQuNzcsMTkyLjA1LDMwNS42MiwxNjkuOTUsMjg5LDE1My4zM3ogTTIyNi43NywxMjcuNTZjLTIwLjQ3LDAtMzcuMTItMTYuNjUtMzcuMTItMzcuMTIKCWMwLTIwLjQ3LDE2LjY1LTM3LjEyLDM3LjEyLTM3LjEyczM3LjEyLDE2LjY1LDM3LjEyLDM3LjEyQzI2My45LDExMC45MSwyNDcuMjQsMTI3LjU2LDIyNi43NywxMjcuNTZ6Ii8+Cjwvc3ZnPgo=" /></div>
            <p><slot></slot></p>
            
        `;
        this._shadow = shadowRoot;
    } 

}
customElements.define('photo-card', PhotoCard);