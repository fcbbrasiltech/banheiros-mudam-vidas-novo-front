export class SimpleButton extends HTMLElement{
    static get observedAttributes() {
        return [];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                :host{
                    margin: 0 auto;
                    display: block;
                    text-align:center;
                    color:white;
                }
                /*COPIA PARA EDGE*/
                simple-button{
                    margin: 0 auto;
                    display: block;
                    text-align:center;
                    color:white;
                }

                :host #container{
                    text-transform: uppercase;
                    font-size: var(--textSizeSmallerHeading);
                    background-color: var(--colorLightBlue);
                    padding: .7em;
                    cursor: pointer;
                    display:inline-block;
                    margin:1em;
                }
                /*COPIA PARA EDGE*/
                simple-button #container{
                    text-transform: uppercase;
                    font-size: var(--textSizeSmallerHeading);
                    background-color: var(--colorLightBlue);
                    padding: .7em;
                    cursor: pointer;
                    display:inline-block;
                    margin:1em;
                }

                :host a:link,:host a:visited ,:host a:hover ,:host a:active{
                    color: inherit;
                }
                /*COPIA PARA EDGE*/
                simple-button a:link,simple-button a:visited ,simple-button a:hover ,simple-button a:active{
                    color: inherit;
                }
            </style>
            <a href="${this.hasAttribute('href') ? this.getAttribute('href') : ''}" target="${this.hasAttribute('target') ? this.getAttribute('target') : ''}">
                <div id="container">
                    <slot></slot>
                </div>
            </a>
        `;
        this._shadow = shadowRoot;
    } 

}
customElements.define('simple-button', SimpleButton);