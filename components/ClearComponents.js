export class ClearComponent extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style></style>
            <div></div>
        `;
        shadowRoot=document.querySelector("<templ>")
        this._shadow = shadowRoot;
    }
    connectedCallback() {
        console.log('Custom square element added to page.');
    }

    disconnectedCallback() {
        console.log('Custom square element removed from page.');
    }

    adoptedCallback() {
        console.log('Custom square element moved to new page.');
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Custom square element attributes changed.');
    }
}
customElements.define('clear-component', ClearComponent);