export class ExpandInfo extends HTMLElement{
    static get observedAttributes() {
        return ['index'];
    }

    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/`
            <style>
                
                :host{
                    display:block;
                    font-size:initial;
                    color:var(--colorBlack);
                }
                /*COPIA PARA EDGE*/
                expand-info{
                    display:block;
                    font-size:initial;
                    color:var(--colorBlack);
                }
                

                ::slotted(div[slot=title]){
                    transform: translateY(.5em);
                    font-size: var(--textSizeBody);
                    teft-align:left;
                }
                /*COPIA PARA EDGE*/
                expand-info>div[slot=title]{
                    transform: translateY(.5em);
                    font-size: var(--textSizeBody);
                    teft-align:left;
                }

                ::slotted(div[slot=text]){
                    font-size:var(--textSizeBody);
                    padding: var(--sidePadding);
                    color:var(--colorBlack);
                }
                /*COPIA PARA EDGE*/
                expand-info>div[slot=text]{
                    font-size:var(--textSizeBody);
                    padding: var(--sidePadding);
                    color:var(--colorBlack);
                }

                :host([active]) #text-container{
                    max-height:var(--newHeight, 50px);
                }
                /*COPIA PARA EDGE*/
                expand-info[active] #text-container{
                    max-height:var(--newHeight, 50px);
                }

                #text-container{
                    box-sizing:border-box;
                    max-height:0px;
                    overflow:hidden;
                    transition: max-height .5s;
                }

                #visible{
                    display: grid;
                    grid-template-columns: minmax(2.5em, auto) 1fr;
                    align-items: center;
                    position:relative;
                    font-size:initial;
                    grid-gap:var(--sidePadding);
                }

                #visible:after{
                    content: " ";
                    position: absolute;
                    height:1px;
                    background-color: var(--colorLightBlue);
                    display:block;
                    right:0px;
                    width:calc(100% - 1.1em);
                    bottom:0;
                }

                #index{
                    font-size: calc(var(--textSizeMajor) * 1.2);
                    transform: translateY(.2em);
                    font-weight:900;
                    color:var(--colorLightBlue);
                }

                #action{
                    position:absolute;
                    bottom:-.75em;
                    right:0;
                    width:1.5em;
                    height:1.5em;
                    background-color:var(--colorLightBlue);
                    border-radius:50%;
                    color:white;
                    text-align:center;
                    line-height:1.5em;
                    z-index:2;
                    box-sizing: border-box;
                    padding-top:.1em;
                    font-size:1.1em;
                    cursor:pointer;
                }

            </style>
            <div>
                <div id="visible">
                    <span id="index"></span>
                    <slot name="title"></slot>
                    <div id="action">+</div>
                </div>
                
               <div id="text-container">
                    <slot name="text"></slot>
               </div>

               

            </div>
        `;
        this._shadow = shadowRoot;
        this.index=this._shadow.querySelector("#index");
        this.action=this._shadow.querySelector("#action");

        this.action.addEventListener("click", this.disableOthers.bind(this));

        this._slot = this._shadow.querySelector('slot[name=text]');
        this._slot.addEventListener('slotchange', this.slotChange.bind(this));
        window.addEventListener("resize", this.changeHeight.bind(this));
    }

    slotChange(e){
        if(this._slot.assignedElements){
            this.nodes=this._slot.assignedElements();      
        }else{
            let els=this._slot.assignedNodes();
            this.nodes=els.filter(a=>{
                let name=a.constructor.name;
                let r=false;

                if(name!="Text"){
                    r=true;
                }
                return r
            })
        }  

        this.changeHeight();
    }

    changeHeight(){
        this._shadow.querySelector("#text-container").style.setProperty("--newHeight", this.nodes[0].clientHeight+"px");    
    }

    disableOthers(){
        if(this.hasAttribute("active")){
            this.removeAttribute("active");
        }else{
            var event = new CustomEvent("disableOthers", {});
            this.dispatchEvent(event);
            this.setAttribute("active", "");
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if(name=="index" && this.index){
            this.index.innerHTML=newValue;
        }
    }
}
customElements.define('expand-info', ExpandInfo);