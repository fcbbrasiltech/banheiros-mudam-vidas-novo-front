export class ImagedInfoGroup extends HTMLElement{
    constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/ `
            <style>
                :host{
                    font-size: 1em;
                    display: grid;
                    box-sizing:border-box;
                    color:var(--colorBlack);
                    grid-template-columns: 35% 65%;
                }
                /*COPIA PARA EDGE*/
                imaged-info-group{
                    font-size: 1em;
                    display: grid;
                    box-sizing:border-box;
                    color:var(--colorBlack);
                    grid-template-columns: 35% 65%;
                }

                :host([right]){
                    grid-template-columns: 65% 35%;
                }
                /*COPIA PARA EDGE*/
                imaged-info-group[right]{
                    grid-template-columns: 65% 35%;
                }

                ::slotted(info-group){
                    padding: 0 var(--sidePadding);
                }
                /*COPIA PARA EDGE*/
                imaged-info-group>info-group{
                    padding: 0 var(--sidePadding);
                }

                :host([right]) ::slotted(reference-image){
                    order:2;
                    background-position: left top;
                }
                /*COPIA PARA EDGE*/
                imaged-info-group[right]>reference-image{
                    order:2;
                    background-position: left top;
                }

            </style>

            <slot></slot>
            <slot></slot>
        `;
        this._shadow = shadowRoot;
    }
}
window.customElements.define('imaged-info-group', ImagedInfoGroup);