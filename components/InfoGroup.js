export class InfoGroup extends HTMLElement{
    static get observedAttributes() {
        return ['data'];
    }

    constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.innerHTML = /*html*/ `
            <style>
                :host{
                    display: inline-block;
                    box-sizing:border-box;
                    

                    --firstColor: var(--colorBlack);
                    --secondColor: var(--colorLightBlue);

                    color:var(--firstColor);
                }
                /*COPIA PARA EDGE*/
                info-group{
                    display: inline-block;
                    box-sizing:border-box;
                    

                    --firstColor: var(--colorBlack);
                    --secondColor: var(--colorLightBlue);

                    color:var(--firstColor);
                }

                :host([dark-bg]){
                    --firstColor: white;
                    --secondColor: var(--colorLightBlue);
                }
                /*COPIA PARA EDGE*/
                info-group[dark-bg]{
                    --firstColor: white;
                    --secondColor: var(--colorLightBlue);
                }

                :host([light-bg]){
                    --firstColor: var(--colorBlack);
                    --secondColor: white;
                }
                /*COPIA PARA EDGE*/
                info-group[light-bg]{
                    --firstColor: var(--colorBlack);
                    --secondColor: white;
                }

                ::slotted(span){
                    display:block;
                }
                /*COPIA PARA EDGE*/
                info-group>span{
                    display:block;
                }

                ::slotted(span:not(:last-child)){
                    margin-bottom: 12px;
                }
                 /*COPIA PARA EDGE*/
                info-group>span:not(:last-child){
                    margin-bottom: 12px;
                }

                ::slotted(span[slot=pre]), ::slotted(span[slot=pos]){
                    text-transform: uppercase;
                    font-size: var(--textSizeSmallerHeading);
                }
                /*COPIA PARA EDGE*/
                info-group>span[slot=pre], info-group>span[slot=pos]{
                    text-transform: uppercase;
                    font-size: var(--textSizeSmallerHeading);
                }

                ::slotted(span[slot=major]){
                    color:var(--secondColor);
                    font-weight: 900;
                    text-transform: uppercase;
                    font-size: var(--textSizeSmallerMajor);
                }
                /*COPIA PARA EDGE*/
                info-group>span[slot=major]{
                    color:var(--secondColor);
                    font-weight: 900;
                    text-transform: uppercase;
                    font-size: var(--textSizeSmallerMajor);
                }

                ::slotted(span[slot=text]){
                    font-weight: 100;
                    font-size: var(--textSizeSmallerBody);
                    line-height: 1.5em;
                }
                /*COPIA PARA EDGE*/
                info-group>span[slot=text]{
                    font-weight: 100;
                    font-size: var(--textSizeSmallerBody);
                    line-height: 1.5em;
                }

                ::slotted(span[slot=source]){
                    font-size: var(--textSizeSmallerSource);
                    font-weight: 100;
                }
                /*COPIA PARA EDGE*/
                info-group>span[slot=source]{
                    font-size: var(--textSizeSmallerSource);
                    font-weight: 100;
                }

                :host([bigger]){
                    margin: 0 auto;
                    display: block;
                }
                /*COPIA PARA EDGE*/
                info-group[bigger]{
                    margin: 0 auto;
                    display: block;
                }

                :host([center]) ::slotted(span){
                    text-align:center;
                }
                /*COPIA PARA EDGE*/
                info-group[center] span[slot]{
                    text-align:center;
                }


                :host([bigger]) ::slotted(span[slot=pre]), :host([bigger]) ::slotted(span[slot=pos]){
                    font-size: var(--textSizeHeading);
                }
                /*COPIA PARA EDGE*/
                info-group[bigger]>span[slot=pre], info-group[bigger]>span[slot=pos]{
                    font-size: var(--textSizeHeading);
                }
                
                :host([bigger]) ::slotted(span[slot=major]){
                    font-size: var(--textSizeMajor);
                }
                /*COPIA PARA EDGE*/
                info-group[bigger]>span[slot=major]{
                    font-size: var(--textSizeMajor);
                }

            </style>

            <slot name="pre"></slot>
            <slot name="major"></slot>
            <slot name="pos"></slot>
            <slot name="text"></slot>
            <slot name="source"></slot>
        `;
        this._shadow = shadowRoot;
    }
}
window.customElements.define('info-group', InfoGroup);